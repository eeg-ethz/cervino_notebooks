# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: run forward simulations with Salvus</h1>
# </div>

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from collections import namedtuple

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
import segyio

# Imports for Cervino.
from cervino.core import create_workflow_dict, SolverSite, Cervino

# <div class="alert alert-info">
#     <h2>Cervino setup</h2>
# </div>

# General `cervino` class which allows one to do most of the work. This class has methods to create the solver input file, to run the simulation, ...

workflow_type = 'forward'

workflow_dict = create_workflow_dict(workflow_type)

workflow_dict['compute_forward_simulation'] = SolverSite('salvus', 'aug04')

pprint(workflow_dict)

cervino_dict = {
    'workflow_type': workflow_type,
    'workflow_dict': workflow_dict,
    'name': 'marmousi_salvus',
    'desc': 'Simple Salvus forward simulation on the Marmousi model',
    'output_folder': 'output'
}

pprint(cervino_dict)

marmousi = Cervino(**cervino_dict)

marmousi

# Construct the dictionaries for the tasks that will compose the forward simulation pipe. This is done in another Notebook. Here, we load the tasks into the pipeline of the Cervino simulation:

tasks_from_pickle = 'tasks.pkl'
marmousi.workflow.pipeline['compute_forward_simulation'].attach_tasks_from_pickle(tasks_from_pickle)

marmousi.workflow.pipeline['compute_forward_simulation'].get_tasks_dict()['0000']['general']['solver'] = 'salvus'

marmousi.workflow.pipeline['compute_forward_simulation'].get_tasks_dict()['0000']['general']['solver']

summary = marmousi.summary()
for line in summary:
    print(line)

# <div class="alert alert-info">
#     <h2>Validate simulation - NOT yet implemented</h2>
# </div>

# Before running the simulation, we want to validate its input to make sure it is correct:

# +
# marmousi.validate()
# -

# <div class="alert alert-info">
#     <h2>Run simulation</h2>
# </div>

# We first need to define a small dictionary with a few required parameters:

run_dict = {
    'progress': True,
    'verbose': True,
    'n_ranks': 8,
    'max_ranks': 16,
}

# +
# tasks = marmousi.workflow.pipeline['compute_forward_simulation']
# -

# ## 'gather_0000': ('gather', 'output/job_0000/gather0'),

result, output_files_hdf5 = marmousi.run(run_dict)

# We can print the standard output (or standard error) produced by the simulation:

fields_salvus = ['phi',]
if 'phi_t' in fields_salvus:
    print('Ciaone!')

import pickle
with open('salvus_0000.pkl', 'rb') as f:
    s = pickle.load(f)

pprint(s)

locs = np.array([i['location'] for i in s['output']['point-data']['receiver']])

locs.shape

# +
# # Choose a simulation
# i = 0
# # 0 for standard output and 1 for standard error
# out_err = 0 
# pprint(result['compute_forward_simulation'][i][out_err].decode("utf-8"))
# -

# The output files created by each Matterhorn simulation are gathered into one larger HDF5 file:

pprint(output_files_hdf5)

# <div class="alert alert-info">
#     <h2>Show results</h2>
# </div>

from salvus_toolbox import toolbox

# +
# Generate a shotgather from and HDF5 receiver file.
data, dt, extent = toolbox.get_shotgather("output/job_0000/receiversss.h5", cmp=1, field="gradient-of-phi")
print(f"dt is {dt}s")
print(data.shape)

# Normalize and plot the shotgather.
clip_min = -0.02 * data.max()
clip_max = +0.02 * data.max()
f = plt.figure(figsize=(15, 5))
plt.imshow(data, vmin=clip_min, vmax=clip_max, extent=extent, aspect="auto", cmap=plt.cm.seismic)

plt.xlabel("Offset (m)")
plt.ylabel("Time (s)")
plt.show()
# -

tri2, data = toolbox.visualize_wavefield_2d('output/job_0000/wavefield.h5', 'gradient-of-phi', cmp=0)

data.shape

clip=1e0
plt.tricontourf(tri2, data[20, :], cmap=plt.cm.seismic, vmin=-clip, vmax=clip)





import xarray as xr

# First, we load one of the HDF5 output file as an `xarray.Dataset`:

ds = xr.load_dataset('0000.h5')

# We now list the `xarray.DataArray`s included in the dataset:

ds.data_vars

# We show the coordinates associated with the datasets:

ds.coords

# Now we plot a time slice:

da = ds['slice_time_0000']

clip = 1e-9
vmin, vmax = -clip, clip
_ = da[da.time_slice==0.8, da.x>3000, :].T.plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# Now we plot a DFT slice:

da = ds['slice_dft_0000']

clip = 1e-2
vmin, vmax = -clip, clip
_ = da[da.freq_dft==20, :, da.x>400].plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# Finally, we plot a shot gather:

da = ds['gather_0000']

clip = 5e-10
vmin, vmax = -clip, clip
# _ = da[da.time_gather>0.1, da.recx>1000].plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)
_ = da[:, (da.recx>=300) & (da.recx<=8900)].plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)


