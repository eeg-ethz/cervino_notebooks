# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# # Marmousi
#
# From the official [tutorial](https://mondaic.com/docs/tutorials/exploration/marmousi_2d/marmousi_2d).

# +
# %matplotlib inline
# %config Completer.use_jedi = False

import wget
import shutil
from pathlib import Path
from typing import List

import scipy
import obspy
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt

from salvus.flow import api
from salvus.flow import simple_config as config

from salvus_toolbox import toolbox
# -

# Download Marmousi model ~150 MB.
url = (
    "https://s3.amazonaws.com/open.source.geoscience/open_data"
    "/elastic-marmousi/elastic-marmousi-model.tar.gz"
)
if not Path("elastic-marmousi-model.tar.gz").exists():
    shutil.unpack_archive(wget.download(url))

# Read marmousi model.
model_directory = Path("./elastic-marmousi-model/model")
marmousi_model = toolbox.read_elastic_marmousi(model_directory, ["VP", "RHO"])

marmousi_model

marmousi_model['VP'].data.shape

marmousi_model['VP'].data.flags

marmousi_model['VP'][2][0].data.__array_interface__['data']

marmousi_model["VP"].plot(aspect="auto", figsize=(15, 5))
plt.show()
marmousi_model["RHO"].plot(aspect="auto", figsize=(15, 5))
plt.show()

# +
wavelet = config.stf.Ricker(center_frequency=5.0)

f, ax = plt.subplots(1, 2, figsize=(15, 5))

ax[0].plot(*wavelet.get_stf())
ax[0].set_xlabel("Time (s)")
ax[0].set_ylabel("Amplitude")

ax[1].plot(*wavelet.get_power_spectrum())
ax[1].set_xlabel("Frequency (Hz)")
ax[1].set_ylabel("Amplitude")

plt.show()
# -

num_absorbing_layers = 10
absorbing_side_sets = ["x0", "x1", "y0"]

mesh_frequency = 2 * wavelet.center_frequency
salvus_mesh = toolbox.mesh_from_xarray(
    model_order=4,
    data=marmousi_model,
    slowest_velocity="VP",
    maximum_frequency=mesh_frequency,
    elements_per_wavelength=1.5,
    absorbing_boundaries=(absorbing_side_sets, num_absorbing_layers),
)

salvus_mesh

source = config.source.cartesian.ScalarPoint2D(
    source_time_function=wavelet, x=8500.0, y=3490.0, f=1
)

receivers = config.receiver.cartesian.SideSetHorizontalPointCollection2D(
    x=np.linspace(0.0, 17000.0, 1000),
    offset=-10.0,
    station_code="xx",
    side_set_name="y1",
    fields=["phi", "gradient-of-phi"],
)

s = config.simulation.Waveform(
    mesh=salvus_mesh, sources=source, receivers=receivers
)

# +
# Leave start time and time step as their auto-detected defaults.
# s.physics.wave_equation.start_time_in_seconds = ?
# s.physics.wave_equation.time_step_in_seconds = ?

# Set end time.
s.physics.wave_equation.end_time_in_seconds = 8.0

# +
# Define Dirichlet boundary at top surface (pressure-free).
dirichlet = config.boundary.HomogeneousDirichlet(side_sets=["y1"])

# Define coupled Clayton-Enqist / Kosloff
# damping boundaries at the the remaining side-sets.
absorbing = config.boundary.Absorbing(
    width_in_meters=685.333,
    side_sets=["x0", "y0", "x1"],
    taper_amplitude=wavelet.center_frequency,
)

# Add the boundaries to the parameter file.
s.physics.wave_equation.boundaries = [absorbing, dirichlet]
# -

s.output.point_data.format = "hdf5"

# +
# s.output.volume_data.format = "hdf5"
# s.output.volume_data.filename = "output.h5"
# s.output.volume_data.fields = ["phi", "gradient-of-phi"]
# s.output.volume_data.sampling_interval_in_time_steps = 100
# -

s.validate()

api.run(
    input_file=s,
    ranks=2,
    site_name="aug04",
    get_all=True,
    output_folder="output",
    overwrite=True,
)

# +
# Generate a shotgather from and HDF5 receiver file.
data, dt, extent = toolbox.get_shotgather("output/receivers.h5", field="phi")

# Normalize and plot the shotgather.
clip_min = 0.01 * data.min()
clip_max = 0.01 * data.max()
f = plt.figure(figsize=(15, 5))
plt.imshow(data, vmin=clip_min, vmax=clip_max, extent=extent, aspect="auto")

plt.xlabel("Offset (m)")
plt.ylabel("Time (s)")
plt.show()
# -

data.shape

import h5py

f5 = h5py.File("output/receivers.h5", mode='r')

for i in f5.keys():
    print(f5[i])

for i in f5['point'].attrs:
    print(i)
