# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: create dict for forward simulation</h1>
# </div>

# This tutorial shows how to build the Python dictionary required to define a forward simulation. To do so, we will first write a function to create each section separately and then we will build the final dictionary combining all the functions.

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
import json
import pickle

# Imports for Cervino.
from cervino.core import get_general_dict, get_time_dict, get_frequency_dict, get_physics_dict, get_pml_dict
from cervino.utils.models import marmousi_original
from cervino.domain import AcousticDomain
from cervino.input.wavelets import RickerWavelet, UserWavelet
from cervino.input.sources import PointSource    
from cervino.output.receivers import Gather
from cervino.output.slices import TimeSlice, DFTSlice

# <div class="alert alert-info">
#     <h2>Cervino dictionary sections</h2>
# </div>

# <div class="alert alert-info">
#     <h3>General section</h3>
# </div>

# In this initial section, we define a few general parameters required by the simulation. Since we want to perform a **forward simulation**, we set `task_type` equal to `compute_forward_simulation`. First, we select the specific solver for this simulation.

solver = 'salvus'


def cervino_general_dict(uid: str):
    return get_general_dict(uid=uid,
                                 task_type='compute_forward_simulation',
                                 solver=solver,
                                 desc=f"{solver} and marmousi")


# <div class="alert alert-info">
#     <h3>Domain section</h3>
# </div>

# In this section, we need to define the domain used for the simulation: geometries and model parameters representing the specific portion of the subsurface that we want to study. In the example below, we choose the original Marmousi model. First thing, we need to obtain the binary files from the internet.

dataset, fn_h5 = marmousi_original(solver=solver, overwrite=True, verbose=True)
print(f"The marmousi file name is '{fn_h5}'")

# Make the model homogeneous
dataset['VP'].data = np.ones_like(dataset['VP'].data) * 2800
dataset['RHO'].data = np.ones_like(dataset['RHO'].data) * 2000

fn_prefix = 'marmousi_original_model/marmousi_original'
dataset['VP'].transpose('x', 'z').data.astype('float32').tofile(f"{fn_prefix}_{'vp.bin'}")
dataset['RHO'].transpose('x', 'z').data.astype('float32').tofile(f"{fn_prefix}_{'rho.bin'}")

# We then set the velocity and density models directly from the HDF5 created by the utility above:

# +
# We instantiate an AcousticDomain object:
domain = AcousticDomain()

# Set the models
domain.set_model_from_xarray(dataset, fn_h5=fn_h5)

def cervino_domain_dict():
    return domain.get_domain_dict()


# -

# <div class="alert alert-info">
#     <h3>Time and frequency sections</h3>
# </div>

# Here we need to define the time step in seconds `time_step_in_seconds` and the number of time steps `number_of_timesteps`. In the example below, we first define the time step, initial, and final time, and use these values to compute the number of time steps. However, one could also define the number of time steps directly.

# +
time_step_in_seconds = 4e-4
start_time_in_seconds = 0.0
# time_shift_in_seconds = 1.0
end_time_in_seconds = 2.0

# Time axis
t = np.arange(start_time_in_seconds, end_time_in_seconds, time_step_in_seconds)

number_of_timesteps = t.size

def cervino_time_dict():
    return get_time_dict(ts=time_step_in_seconds, nt=number_of_timesteps)


# -

# <div class="alert alert-info">
#     <h3>Source section</h3>
# </div>

# Here, we define the input source which will generate the wavefields. In this tutorial, we define a point source characterised by a Ricker wavelet as source time function.

# +
# Use a Ricker wavelet as source time function
center_frequency = 20.0
stf = RickerWavelet(t, fc=center_frequency, delay=1.5, delay_type='cycles', create_binary=True, fn_prefix='source')

# Additionally, we can also load a user-defined wavelet from a binary file.
# filename = 'user_defined_ricker_20hz.bin'
# stf = UserWavelet(filename, nt=t.size)

# Define a point source
name = "source0"
spatial_type = "fz"
scale = 1.

def cervino_source_dict(uid: str=None, location: list=None):
    return PointSource(loc=location, stf=stf, spatial_type=spatial_type).get_source_dict()


# +
# We compute the FFT of the source time function.
# The values of the FFT at specific frequencies will be required later when defining DFT slice outputs.

# Frequency step in Hz
df = 1 / (time_step_in_seconds * number_of_timesteps)
Nfft = stf.wavelet.size

# Frequency axis
f = np.arange(Nfft) * df

# FFT
W = np.fft.fft(stf.wavelet)

# Plot
plt.plot(f, np.abs(W))
_ = plt.title('FFT of Ricker wavelet')
_ = plt.xlabel('Frequency [Hz]')
_ = plt.xlim([0, 100])


# -

# <div class="alert alert-info">
#     <h3>Output section</h3>
# </div>

# Here, we define the output of the simulation. First, we create two shot gathers:

def cervino_gather_dict(uid=None):
    # First we create a general gather object
    start_timestep = 0
    end_timestep = len(t) - 1
    timestep_increment = 10

    gather = Gather(start_timestep=start_timestep, end_timestep=end_timestep, timestep_increment=timestep_increment, fmt='su')

    n = domain.get_domain_dict()['geometry']['number-of-cells']
    d = domain.get_domain_dict()['geometry']['cell-size']

    # Number of receivers
    nr = n[0]
    # Receiver z-value
    rz0 = 40.0
    # x-value of first receiver
    rx0 = 0.0
    # x-value of last receiver
    rx1 = nr * d[0]

    # Then we define multiple receiver arrays
    rec0 = gather.add_receivers(uid, origin=(rx0, 0.0, rz0), increment=(d[0], 0.0, 0.0), nr=nr, attribute='V01Z', filename="vz_gather")
    rec1 = gather.add_receivers(uid, origin=(rx0, 0.0, rz0), increment=(d[0], 0.0, 0.0), nr=nr, attribute='V10X', filename="vx_gather")
    
    return gather.get_receivers_dict()


# Additionally, we create time snapshots:

def cervino_slice_dict(uid=None):
    # First we create a general snapshot object
    start_timestep = 0
    end_timestep = len(t) - 1
    timestep_increment = 200

    snapshot = TimeSlice(start_timestep=start_timestep, end_timestep=end_timestep, timestep_increment=timestep_increment, fmt='su')

    # Axis
    slice_axis = 'y'
    # Slice index
    slice_index = 0

    # Then we define two slices
    slice0 = snapshot.add_slices(uid, axis=slice_axis, slice_index=slice_index, attribute='V10X', filename="vx_slice")
    slice1 = snapshot.add_slices(uid, axis=slice_axis, slice_index=slice_index, attribute='V01Z', filename="vz_slice")
    
    return snapshot.get_slices_dict()


# Additionally, we create DFT slices:

def cervino_dft_dict(uid=None):
    # First we create a general snapshot object
    dft_frequencies = list(range(5, 81, 5))

    # Write wavelet coefficients to file
    dft_wavelet_coefficients = np.array([W[int(i / df)] for i in dft_frequencies], dtype=np.complex64)
    dft_wavelet_coefficients_fn = f"dft_wavelet_coefficients_source0.bin"
    dft_wavelet_coefficients.tofile(dft_wavelet_coefficients_fn)
    # np.array(dft_frequencies, np.float32).tofile(dft_wavelet_coefficients_fn)

    dft = DFTSlice(dft_frequencies=dft_frequencies, dft_wavelet_coefficients=dft_wavelet_coefficients_fn, fmt='su')

    # We define a DFT slice:

    # Axis
    slice_axis = 'y'
    # Slice index
    slice_index = 0

    dft_slice0 = dft.add_slices(uid, axis=slice_axis, slice_index=slice_index, attribute='S00XX', filename="sxx_dft_slice")
    
    return dft.get_slices_dict()


# <div class="alert alert-info">
#     <h3>Physics section</h3>
# </div>

# +
# We define the PML boundary conditions:
pml_dict = get_pml_dict(fs=True, width=10, power=4, frequency=center_frequency, vel=3000.)

# We define the order of the spatial stencil:
def cervino_physics_dict():
    return get_physics_dict(order=4, boundaries=[pml_dict,])


# -

# <div class="alert alert-info">
#     <h3>Assemble complete cervino dictionary</h3>
# </div>

# Generate the complete input dictionary.
def compute_forward_simulation_get_dict(uid, source_location):
    return {
        "general": cervino_general_dict(uid),
        "domain": cervino_domain_dict(),
        "time": cervino_time_dict(),
        "physics": cervino_physics_dict(),
        "source": {
            "point-source": cervino_source_dict(uid, source_location),
        },
        "output": {
            "gather": cervino_gather_dict(uid),
            "slice_time": cervino_slice_dict(uid),
            "slice_dft": cervino_dft_dict(uid),
        }
    }


# <div class="alert alert-info">
#     <h2>Define source locations - 2D line</h2>
# </div>

# We want to simulate multiple shots. Each simulation corresponds to a *task* within the sime *pipe*. In this tutorial, we are building the **compute_forward_simulation** pipe. This *pipe* could be the only *pipe* within a more complex *pipeline*. We now define the location of the shots, three in this example.

locations = [(x, 0.0, 20.0) for x in range(2000, 6001, 1500)]
pprint(locations)

locations = locations[0],

# <div class="alert alert-info">
#     <h2>Create final cervino dictionary</h2>
# </div>

# In this example, we want to compose the dictionary for three forward simulations: one simulation for each shot location. Each simulation corresponds to a *task* of a more complex *pipeline*. A *pipeline* is composed of *pipes*. In this specific tutorial, we are defining the *tasks* of the **compute_forward_simulation** *pipe*. Each *task* is identified by a *uid* (unique identifier). The *uid* is a string.

uid = "{:04d}"
tasks = {uid.format(i): compute_forward_simulation_get_dict(uid.format(i), loc) for i, loc in enumerate(locations)}

# The dictionary `tasks` (created in the cell above) is a dictionary of dictionaries. They `keys` of `tasks` are the *uids* of the three simulations:

pprint(tasks.keys())

# The dictionary of the simulation corresponding to *uid* `0001` is:

pprint(tasks['0000'])

# <div class="alert alert-info">
#     <h2>Export the cervino dictionary</h2>
# </div>

# A dictionary can be serialized and saved to a file using the [pickle](https://docs.python.org/3/library/pickle.html) module. This allows us to load the `tasks` dictionary from another Jupyter Notebook or Python script.

output_file = 'tasks.pkl'
with open(output_file, 'wb') as f:
    pickle.dump(tasks, f)

# To check that the dictionary was correctly exported, we load it again from file and print the same dictionary as above:

with open(output_file, 'rb') as f:
    tasks_from_pickle = pickle.load(f)

pprint(tasks_from_pickle['0000'])

# +
# differences = {k: tasks_from_pickle[k] for k in set(tasks_from_pickle) - set(tasks)}
# pprint(differences)
