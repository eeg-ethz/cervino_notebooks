{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fourier Transform on the Fly\n",
    "\n",
    "This notebook shows how to Fourier transform the wavefield on the fly during the simulation and to output the wavefield in frequency domain at a set of discrete frequencies.\n",
    "\n",
    "The only API-facing changes compared to previous simulations is a new set of optional output settings in the SalvusFlow `simple_config`.\n",
    "\n",
    "```\n",
    "sim.output.frequency_domain.filename = \"frequency_domain.h5\"\n",
    "sim.output.frequency_domain.format = \"hdf5\"\n",
    "sim.output.frequency_domain.fields = [\"phi\"]\n",
    "sim.output.frequency_domain.frequencies = [50.0, 60.0, 70.0, 100.0]\n",
    "```\n",
    "\n",
    "The real and imaginary parts of the wavefield are stored in the same data format as the volumetric time-dependent wavefield output.\n",
    "\n",
    "**@Filippo:** \n",
    "This is something to keep in mind for building the Jacobian, because then we need to invoke the mapping from wavefield to model space."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "%config Completer.use_jedi = False\n",
    "\n",
    "# Standard Python packages\n",
    "import toml\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# Workflow management.\n",
    "from salvus.flow import api\n",
    "\n",
    "# Specific objects to aid in setting up simulations.\n",
    "from salvus.mesh.simple_mesh import basic_mesh\n",
    "import salvus.flow.simple_config as sc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Domain setup\n",
    "\n",
    "Here, we use a simple 2D acoustic layered model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Overwriting layered_model.bm\n"
     ]
    }
   ],
   "source": [
    "%%writefile layered_model.bm\n",
    "NAME         simple_layers\n",
    "UNITS        m\n",
    "ACOUSTIC     TRUE\n",
    "COLUMNS      depth rho vp\n",
    " 0.0     2600     2000\n",
    " 50.0    2600     2000\n",
    " 50.0    3000     2400\n",
    " 100.0   3000     2400\n",
    " 100.0   2600     2000\n",
    " 150.0   2600     2000\n",
    " 150.0   3400     2800\n",
    " 200.0   3400     2800\n",
    " 200.0   2600     2000\n",
    " 450.0   2600     2000"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "c8c834462ab84b72a0ca030dd9b476cc",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "VBox(children=(HTML(value='\\n            <p><i>This is only a preview. Use ParaView to actually investigate\\n …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<salvus.mesh.unstructured_mesh.UnstructuredMesh at 0x7f352c7bff50>"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "max_x = 800.0\n",
    "max_frequency = 150.0  # Frequency in Hz.\n",
    "mesh_config = basic_mesh.CartesianFromBm2D(bm_file='layered_model.bm', \n",
    "                                           x_max=max_x, y_max=450.0,\n",
    "                                           max_frequency=max_frequency,\n",
    "                                           tensor_order=4)\n",
    "mesh_config.refinement.refinement_bottom_up = True\n",
    "mesh_config.refinement.refinement_style = 'doubling'\n",
    "mesh = mesh_config.create_mesh()\n",
    "mesh"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sources and receivers\n",
    "\n",
    "We use a single scalar point source, emitting a Ricker wavelet with a center frequency of $75$ Hz as a source time function. For receivers, we'll set up a line of 50 equidistantly spaced between $x=200$ m and $x=600$ m, at a vertical position of 450 m. We need to specify a unique station code for each receiver, as well as a list of which fields we'd like the receivers to record (`phi` in this case)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sources.\n",
    "sx, sy = 350.0, 400.0  # Source position (m)\n",
    "stf = sc.source.stf.Ricker(center_frequency=max_frequency/2, time_shift_in_seconds=0.02)\n",
    "source = sc.source.cartesian.ScalarPoint2D(\n",
    "    x=sx, y=sy, f=1.0, source_time_function=stf\n",
    ")\n",
    "\n",
    "# Receivers.\n",
    "nr = 50      # Number of receivers.\n",
    "ry0 = 450.0  # Receiver y-value.\n",
    "rx0 = 200.0  # x-value of first receiver.\n",
    "rx1 = 600.0  # x-value of last receiver.\n",
    "receivers = [\n",
    "    sc.receiver.cartesian.Point2D(\n",
    "        x=x, y=ry0, station_code=f\"{_i:03d}\", fields=[\"phi\"]\n",
    "    )\n",
    "    for _i, x in enumerate(np.linspace(rx0, rx1, nr))\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Input file generation\n",
    "\n",
    "We use the SalvusFlow `simple_config` object to gather all properties of the wavefield simulation. \n",
    "[Here](https://mondaic.com/docs/references/python_apis/salvus_flow/simple_config) is the full list of options, and [here](https://mondaic.com/docs/examples) are several examples for different use cases.\n",
    "\n",
    "As mentioned in the beginning, the only new step is adding a block to specify the frequency domain output.\n",
    "Here, we need to provide the fields we want to transform on the fly (e.g., displacement, velocity, ...), and a set of discrete frequencies for which to run the FFT. The transformed wavefield will be written to the specified output file, and contains real and imaginary part for each of the specified frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "5e2d25e567584fa29ec2e1aafe3fcdf5",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "VBox(children=(HTML(value='\\n            <p><i>This is only a preview. Use ParaView to actually investigate\\n …"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "<salvus.flow.simple_config.simulation.Waveform object at 0x7f3525ccc3d0>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sim = sc.simulation.Waveform(mesh=mesh, sources=source, receivers=receivers)\n",
    "\n",
    "# Modify the start-time, end-time, and time-step of the simulation.\n",
    "sim.physics.wave_equation.end_time_in_seconds = 0.4\n",
    "sim.physics.wave_equation.time_step_in_seconds = 0.0002\n",
    "sim.physics.wave_equation.start_time_in_seconds = 0.0\n",
    "\n",
    "# Add absorbing boundary conditions\n",
    "boundaries = sc.boundary.Absorbing(taper_amplitude=max_frequency/2, \n",
    "                                   width_in_meters=150.0, \n",
    "                                   side_sets=['x0', 'x1', 'y0'])\n",
    "sim.add_boundary_conditions(boundaries)\n",
    "\n",
    "# # Initialize frequency domain output\n",
    "sim.output.frequency_domain.filename = \"frequency_domain.h5\"\n",
    "sim.output.frequency_domain.format = \"hdf5\"\n",
    "sim.output.frequency_domain.fields = [\"phi\"]\n",
    "sim.output.frequency_domain.frequencies = [50.0, 60.0, 70.0, 100.0]\n",
    "\n",
    "# Validate and visualize\n",
    "sim.validate()\n",
    "sim"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'domain': {'dimension': 2,\n",
       "  'polynomial-order': 4,\n",
       "  'mesh': {'filename': '__SALVUS_FLOW_SPECIAL_TEMP__', 'format': 'hdf5'},\n",
       "  'model': {'filename': '__SALVUS_FLOW_SPECIAL_TEMP__', 'format': 'hdf5'},\n",
       "  'geometry': {'filename': '__SALVUS_FLOW_SPECIAL_TEMP__', 'format': 'hdf5'}},\n",
       " 'output': {'meta-data': {'meta-json-filename': 'meta.json',\n",
       "   'progress-json-filename': 'progress.json'},\n",
       "  'point-data': {'filename': 'receivers.h5',\n",
       "   'format': 'asdf',\n",
       "   'sampling-interval-in-time-steps': 1,\n",
       "   'receiver': [{'location': [200.0, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '000',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [208.16326530612244, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '001',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [216.3265306122449, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '002',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [224.48979591836735, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '003',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [232.6530612244898, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '004',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [240.81632653061223, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '005',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [248.9795918367347, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '006',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [257.14285714285717, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '007',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [265.3061224489796, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '008',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [273.46938775510205, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '009',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [281.63265306122446, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '010',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [289.7959183673469, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '011',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [297.9591836734694, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '012',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [306.1224489795918, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '013',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [314.2857142857143, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '014',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [322.44897959183675, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '015',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [330.61224489795916, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '016',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [338.7755102040816, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '017',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [346.9387755102041, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '018',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [355.1020408163265, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '019',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [363.265306122449, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '020',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [371.42857142857144, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '021',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [379.59183673469386, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '022',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [387.7551020408163, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '023',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [395.9183673469388, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '024',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [404.0816326530612, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '025',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [412.2448979591837, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '026',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [420.40816326530614, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '027',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [428.57142857142856, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '028',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [436.734693877551, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '029',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [444.8979591836735, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '030',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [453.0612244897959, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '031',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [461.2244897959184, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '032',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [469.38775510204084, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '033',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [477.55102040816325, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '034',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [485.7142857142857, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '035',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [493.8775510204082, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '036',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [502.0408163265306, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '037',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [510.2040816326531, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '038',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [518.3673469387755, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '039',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [526.530612244898, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '040',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [534.6938775510205, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '041',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [542.8571428571429, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '042',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [551.0204081632653, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '043',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [559.1836734693877, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '044',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [567.3469387755102, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '045',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [575.5102040816327, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '046',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [583.6734693877552, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '047',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [591.8367346938776, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '048',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']},\n",
       "    {'location': [600.0, 450.0],\n",
       "     'network-code': 'XX',\n",
       "     'station-code': '049',\n",
       "     'location-code': '',\n",
       "     'fields': ['phi']}]},\n",
       "  'volume-data': {'filename': 'wavefield.h5',\n",
       "   'format': 'hdf5',\n",
       "   'fields': ['phi'],\n",
       "   'sampling-interval-in-time-steps': 100},\n",
       "  'frequency-domain': {'filename': 'frequency_domain.h5',\n",
       "   'format': 'hdf5',\n",
       "   'fields': ['phi'],\n",
       "   'frequencies': [50.0, 60.0, 70.0, 100.0]}},\n",
       " 'physics': {'wave-equation': {'time-stepping-scheme': 'newmark',\n",
       "   'start-time-in-seconds': 0.0,\n",
       "   'end-time-in-seconds': 0.4,\n",
       "   'time-step-in-seconds': 0.0002,\n",
       "   'courant-number': 0.6,\n",
       "   'attenuation': False,\n",
       "   'point-source': [{'location': [350.0, 400.0],\n",
       "     'spatial-type': 'scalar',\n",
       "     'spatial-weights': [1.0],\n",
       "     'source-time-function': {'center-frequency': 75.0,\n",
       "      'time-shift-in-seconds': 0.02,\n",
       "      'wavelet': 'ricker'}}],\n",
       "   'boundaries': [{'side-sets': ['x0', 'x1', 'y0'],\n",
       "     'taper-amplitude': 75.0,\n",
       "     'type': 'absorbing',\n",
       "     'width-in-meters': 150.0}]}}}"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sim.get_dictionary()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running a simulation\n",
    "\n",
    "With everything ready to go, it's now time to run the simulation with SalvusFlow. Again, the API is no different from conventional simulations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Job `job_2003031524544201_01447e1c81` running on `aug04` with 4 rank(s).\n",
      "Site information:\n",
      "  * Salvus version: 0.11.0-beta.1-853-g28a5ee38\n",
      "  * Floating point size: 32\n",
      "                                               \r"
     ]
    },
    {
     "data": {
      "application/vnd.jupyter.widget-view+json": {
       "model_id": "cbd7a57a707e40db81627a4b7fd2cebb",
       "version_major": 2,
       "version_minor": 0
      },
      "text/plain": [
       "HBox(children=(IntProgress(value=0, description='Solving forward problem/Computing forward wavefield', layout=…"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\n",
      "* Downloaded 28.2 MB of results to `output`.\n",
      "* Total run time: 9.06 seconds.\n",
      "* Pure simulation time: 6.98 seconds.\n"
     ]
    }
   ],
   "source": [
    "job = api.run(site_name=\"aug04\",\n",
    "                       input_file=sim,\n",
    "                       ranks=4,\n",
    "                       output_folder=\"output\",\n",
    "                       overwrite=True,\n",
    "                       get_all=True,\n",
    "                       )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inspect output\n",
    "\n",
    "The frequency domain output has been written to an `hdf5` file. The accompanying `xdmf` file allows us to visualize the field in Paraview.\n",
    "The line below will only work on MacOS, on Linux you have to manually open the file in Paraview."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# !open output/frequency*.xdmf"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import h5py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f5 = h5py.File(\"output/frequency_domain.h5\", mode='r')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in f5.keys():\n",
    "    print(f5[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2001 time steps\n",
    "8040 elements\n",
    "1 field only\n",
    "32 is 25 rounded to the nearst (higher) power of 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f5['frequency-domain']['phi']`.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = f5['frequency-domain']['phi'][0, :]\n",
    "plt.imshow"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "8040 grid elements and each element has 25 grid points"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f5['coordinates_ACOUSTIC'].shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mesh.write_h5('mesh_marmousi.h5')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from salvus_toolbox.toolbox import visualize_wavefield_2d"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tri, data = visualize_wavefield_2d('output/wavefield.h5', 'phi')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clip=1e2\n",
    "plt.tricontourf(tri, data[3, :], vmin=-clip, vmax=clip)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "from typing import List, Optional, Tuple, Union\n",
    "\n",
    "import h5py\n",
    "import matplotlib.tri as tri"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "def visualize_dft_2d(\n",
    "    filename: Union[str, Path], field: str, cmp: int = 0\n",
    ") -> Tuple[tri.triangulation.Triangulation, np.ndarray]:\n",
    "    \"\"\"\n",
    "    Get an object that can be used to plot simple 2-D wavefields.\n",
    "\n",
    "    This method works by using the tricontourf method in matplotlib, and so\n",
    "    comes with some limitations. For instance, if the mesh is not convex, the\n",
    "    triangulation will fill in any holes and thus lead to unexpected results.\n",
    "    This function is meant to quickly plot wavefields on simple domains for\n",
    "    use in jupyter-notebooks and the like -- for more detailed volumetric\n",
    "    plotting (including in 3-D) please use Paraview.\n",
    "\n",
    "    Args:\n",
    "        filename: Name of volumetric output file.\n",
    "        field: Field to plot.\n",
    "        cmp: Component of the field to plot.\n",
    "\n",
    "    Returns:\n",
    "        A tuple of a triangulation object, followed by the data which can be\n",
    "        plotted with respect to that object, for instance plt.tricontourf(t,\n",
    "        d[ts, :]) will plot the output at time-step ts.\n",
    "\n",
    "    Raises:\n",
    "        ValueError: If a field is completely unknown in Salvus.\n",
    "        ValueError: If a field is not present in the file.\n",
    "        ValueError: If a field is of a different physics than that in the file.\n",
    "    \"\"\"\n",
    "    sf = set((\"phi\", \"phi_t\", \"phi_tt\", \"gradient-of-phi\"))\n",
    "    vf = set(\n",
    "        (\n",
    "            \"displacement\",\n",
    "            \"velocity\",\n",
    "            \"acceleration\",\n",
    "            \"gradient-of-displacement\",\n",
    "            \"strain\",\n",
    "        )\n",
    "    )\n",
    "\n",
    "    if field not in sf and field not in vf:\n",
    "        raise ValueError(\"Unknown field specified.\")\n",
    "    physics = \"scalar\" if field in sf else \"vector\"\n",
    "\n",
    "    crd_base = \"/coordinates_{}\"\n",
    "    coord_names = {\n",
    "        \"scalar\": crd_base.format(\"ACOUSTIC\"),\n",
    "        \"vector\": crd_base.format(\"ELASTIC\"),\n",
    "    }\n",
    "\n",
    "    with h5py.File(str(filename), \"r\") as fh:\n",
    "\n",
    "        # Read coordinates.\n",
    "        if coord_names[physics] in fh:\n",
    "            crds = fh[coord_names[physics]][:]\n",
    "        else:\n",
    "            raise ValueError(\n",
    "                f\"Request to plot a {physics} field, but no {physics} fields \"\n",
    "                f\"were found in {filename}.\"\n",
    "            )\n",
    "\n",
    "        # Generate triangulation.\n",
    "        npts, d = np.product(crds.shape[:2]), crds.shape[2]\n",
    "        triang = tri.Triangulation(\n",
    "            *np.squeeze(np.hsplit(crds.reshape(npts, d), d))\n",
    "        )\n",
    "\n",
    "        v_field = f\"/frequency-domain/{field}\"\n",
    "        if v_field in fh:\n",
    "            return (\n",
    "                triang,\n",
    "                (np.squeeze(fh[v_field][:, :, 0, :25])).reshape(-1, npts),\n",
    "            )\n",
    "        else:\n",
    "            raise ValueError(\n",
    "                f\"Request to plot field {field}, but it was not found in \"\n",
    "                f\"{filename}.\"\n",
    "            )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tri, data = visualize_dft_2d('output/frequency_domain.h5', 'phi')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "clip=5e2\n",
    "plt.tricontourf(tri, data[3, :], cmap=plt.cm.seismic, vmin=-clip, vmax=clip)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,py:light"
  },
  "kernelspec": {
   "display_name": "Python (cervino)",
   "language": "python",
   "name": "cervino"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
