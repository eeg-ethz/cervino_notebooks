# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# # Fourier Transform on the Fly
#
# This notebook shows how to Fourier transform the wavefield on the fly during the simulation and to output the wavefield in frequency domain at a set of discrete frequencies.
#
# The only API-facing changes compared to previous simulations is a new set of optional output settings in the SalvusFlow `simple_config`.
#
# ```
# sim.output.frequency_domain.filename = "frequency_domain.h5"
# sim.output.frequency_domain.format = "hdf5"
# sim.output.frequency_domain.fields = ["phi"]
# sim.output.frequency_domain.frequencies = [50.0, 60.0, 70.0, 100.0]
# ```
#
# The real and imaginary parts of the wavefield are stored in the same data format as the volumetric time-dependent wavefield output.
#
# **@Filippo:** 
# This is something to keep in mind for building the Jacobian, because then we need to invoke the mapping from wavefield to model space.

# ## Python imports

# +
# %matplotlib inline
# %config Completer.use_jedi = False

# Standard Python packages
import toml
import numpy as np
import matplotlib.pyplot as plt

# Workflow management.
from salvus.flow import api

# Specific objects to aid in setting up simulations.
from salvus.mesh.simple_mesh import basic_mesh
import salvus.flow.simple_config as sc
# -

# ## Domain setup
#
# Here, we use a simple 2D acoustic layered model.

# %%writefile layered_model.bm
NAME         simple_layers
UNITS        m
ACOUSTIC     TRUE
COLUMNS      depth rho vp
 0.0     2600     2000
 50.0    2600     2000
 50.0    3000     2400
 100.0   3000     2400
 100.0   2600     2000
 150.0   2600     2000
 150.0   3400     2800
 200.0   3400     2800
 200.0   2600     2000
 450.0   2600     2000

max_x = 800.0
max_frequency = 150.0  # Frequency in Hz.
mesh_config = basic_mesh.CartesianFromBm2D(bm_file='layered_model.bm', 
                                           x_max=max_x, y_max=450.0,
                                           max_frequency=max_frequency,
                                           tensor_order=4)
mesh_config.refinement.refinement_bottom_up = True
mesh_config.refinement.refinement_style = 'doubling'
mesh = mesh_config.create_mesh()
mesh

# ## Sources and receivers
#
# We use a single scalar point source, emitting a Ricker wavelet with a center frequency of $75$ Hz as a source time function. For receivers, we'll set up a line of 50 equidistantly spaced between $x=200$ m and $x=600$ m, at a vertical position of 450 m. We need to specify a unique station code for each receiver, as well as a list of which fields we'd like the receivers to record (`phi` in this case).

# +
# Sources.
sx, sy = 350.0, 400.0  # Source position (m)
stf = sc.source.stf.Ricker(center_frequency=max_frequency/2, time_shift_in_seconds=0.02)
source = sc.source.cartesian.ScalarPoint2D(
    x=sx, y=sy, f=1.0, source_time_function=stf
)

# Receivers.
nr = 50      # Number of receivers.
ry0 = 450.0  # Receiver y-value.
rx0 = 200.0  # x-value of first receiver.
rx1 = 600.0  # x-value of last receiver.
receivers = [
    sc.receiver.cartesian.Point2D(
        x=x, y=ry0, station_code=f"{_i:03d}", fields=["phi"]
    )
    for _i, x in enumerate(np.linspace(rx0, rx1, nr))
]
# -

# ## Input file generation
#
# We use the SalvusFlow `simple_config` object to gather all properties of the wavefield simulation. 
# [Here](https://mondaic.com/docs/references/python_apis/salvus_flow/simple_config) is the full list of options, and [here](https://mondaic.com/docs/examples) are several examples for different use cases.
#
# As mentioned in the beginning, the only new step is adding a block to specify the frequency domain output.
# Here, we need to provide the fields we want to transform on the fly (e.g., displacement, velocity, ...), and a set of discrete frequencies for which to run the FFT. The transformed wavefield will be written to the specified output file, and contains real and imaginary part for each of the specified frequencies.

# +
sim = sc.simulation.Waveform(mesh=mesh, sources=source, receivers=receivers)

# Modify the start-time, end-time, and time-step of the simulation.
sim.physics.wave_equation.end_time_in_seconds = 0.4
sim.physics.wave_equation.time_step_in_seconds = 0.0002
sim.physics.wave_equation.start_time_in_seconds = 0.0

# Add absorbing boundary conditions
boundaries = sc.boundary.Absorbing(taper_amplitude=max_frequency/2, 
                                   width_in_meters=150.0, 
                                   side_sets=['x0', 'x1', 'y0'])
sim.add_boundary_conditions(boundaries)

# # Initialize frequency domain output
sim.output.frequency_domain.filename = "frequency_domain.h5"
sim.output.frequency_domain.format = "hdf5"
sim.output.frequency_domain.fields = ["phi"]
sim.output.frequency_domain.frequencies = [50.0, 60.0, 70.0, 100.0]

# Validate and visualize
sim.validate()
sim
# -

sim.get_dictionary()

# ## Running a simulation
#
# With everything ready to go, it's now time to run the simulation with SalvusFlow. Again, the API is no different from conventional simulations.

job = api.run(site_name="aug04",
                       input_file=sim,
                       ranks=4,
                       output_folder="output",
                       overwrite=True,
                       get_all=True,
                       )


# ## Inspect output
#
# The frequency domain output has been written to an `hdf5` file. The accompanying `xdmf` file allows us to visualize the field in Paraview.
# The line below will only work on MacOS, on Linux you have to manually open the file in Paraview.

# +
# # !open output/frequency*.xdmf
# -

import h5py

f5 = h5py.File("output/frequency_domain.h5", mode='r')

for i in f5.keys():
    print(f5[i])

# 2001 time steps
# 8040 elements
# 1 field only
# 32 is 25 rounded to the nearst (higher) power of 2

f5['frequency-domain']['phi']`.shape

data = f5['frequency-domain']['phi'][0, :]
plt.imshow

# 8040 grid elements and each element has 25 grid points

f5['coordinates_ACOUSTIC'].shape

mesh.write_h5('mesh_marmousi.h5')

from salvus_toolbox.toolbox import visualize_wavefield_2d

tri, data = visualize_wavefield_2d('output/wavefield.h5', 'phi')

clip=1e2
plt.tricontourf(tri, data[3, :], vmin=-clip, vmax=clip)

# +
from pathlib import Path
from typing import List, Optional, Tuple, Union

import h5py
import matplotlib.tri as tri


# -

def visualize_dft_2d(
    filename: Union[str, Path], field: str, cmp: int = 0
) -> Tuple[tri.triangulation.Triangulation, np.ndarray]:
    """
    Get an object that can be used to plot simple 2-D wavefields.

    This method works by using the tricontourf method in matplotlib, and so
    comes with some limitations. For instance, if the mesh is not convex, the
    triangulation will fill in any holes and thus lead to unexpected results.
    This function is meant to quickly plot wavefields on simple domains for
    use in jupyter-notebooks and the like -- for more detailed volumetric
    plotting (including in 3-D) please use Paraview.

    Args:
        filename: Name of volumetric output file.
        field: Field to plot.
        cmp: Component of the field to plot.

    Returns:
        A tuple of a triangulation object, followed by the data which can be
        plotted with respect to that object, for instance plt.tricontourf(t,
        d[ts, :]) will plot the output at time-step ts.

    Raises:
        ValueError: If a field is completely unknown in Salvus.
        ValueError: If a field is not present in the file.
        ValueError: If a field is of a different physics than that in the file.
    """
    sf = set(("phi", "phi_t", "phi_tt", "gradient-of-phi"))
    vf = set(
        (
            "displacement",
            "velocity",
            "acceleration",
            "gradient-of-displacement",
            "strain",
        )
    )

    if field not in sf and field not in vf:
        raise ValueError("Unknown field specified.")
    physics = "scalar" if field in sf else "vector"

    crd_base = "/coordinates_{}"
    coord_names = {
        "scalar": crd_base.format("ACOUSTIC"),
        "vector": crd_base.format("ELASTIC"),
    }

    with h5py.File(str(filename), "r") as fh:

        # Read coordinates.
        if coord_names[physics] in fh:
            crds = fh[coord_names[physics]][:]
        else:
            raise ValueError(
                f"Request to plot a {physics} field, but no {physics} fields "
                f"were found in {filename}."
            )

        # Generate triangulation.
        npts, d = np.product(crds.shape[:2]), crds.shape[2]
        triang = tri.Triangulation(
            *np.squeeze(np.hsplit(crds.reshape(npts, d), d))
        )

        v_field = f"/frequency-domain/{field}"
        if v_field in fh:
            return (
                triang,
                (np.squeeze(fh[v_field][:, :, 0, :25])).reshape(-1, npts),
            )
        else:
            raise ValueError(
                f"Request to plot field {field}, but it was not found in "
                f"{filename}."
            )


tri, data = visualize_dft_2d('output/frequency_domain.h5', 'phi')

clip=5e2
plt.tricontourf(tri, data[3, :], cmap=plt.cm.seismic, vmin=-clip, vmax=clip)


