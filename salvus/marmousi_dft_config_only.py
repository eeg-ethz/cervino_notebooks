# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# # Marmousi
#
# From the official [tutorial](https://mondaic.com/docs/tutorials/exploration/marmousi_2d/marmousi_2d).

# +
# %matplotlib inline
# %config Completer.use_jedi = False

import wget
import shutil
from pathlib import Path
from typing import List

import scipy
import obspy
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt

from salvus.flow import api
from salvus.flow import simple_config as config

from salvus_toolbox import toolbox
# -

from pprint import pprint

# Load the `xarray.Dataset` created with cervino:

marmousi_model = xr.load_dataset('../cervino/marmousi_original_model/marmousi_original.h5')

# Rename the `z` axis to `y` and then reverse it:

marmousi_model = marmousi_model.rename({
    "z": "y",
})

marmousi_model.coords['y'].data = marmousi_model.coords['y'].data[::-1]

marmousi_model

# Define the source wavelet:

wavelet = config.stf.Ricker(center_frequency=20.0)

# Define the absorbing boundary conditions on the left, right, and bottom sides:

num_absorbing_layers = 10
absorbing_side_sets = ["x0", "x1", "y0"]

# Build the model mesh from the input `xarray.Dataset`:

mesh_frequency = 2 * wavelet.center_frequency
salvus_mesh = toolbox.mesh_from_xarray(
    model_order=4,
    data=marmousi_model,
    slowest_velocity="VP",
    maximum_frequency=mesh_frequency,
    elements_per_wavelength=1.5,
    absorbing_boundaries=(absorbing_side_sets, num_absorbing_layers),
)

# Define the source type and location:

source = config.source.cartesian.ScalarPoint2D(
    source_time_function=wavelet, x=2000.0, y=2980.0, f=1
)

# Define the receivers:

x = np.arange(2301) * 4.0

x[(x>absorbing.width_in_meters) & (x<x[-1] - absorbing.width_in_meters)]

receivers = config.receiver.cartesian.SideSetHorizontalPointCollection2D(
    x=x,
    offset=-40.0,
    station_code="xx",
    side_set_name="y1",
    fields=["gradient-of-phi"],
)

# +
# s = config.simulation.Waveform(
#     mesh=salvus_mesh, sources=source, receivers=receivers
# )
# -

# Create the `Waveform` simulation:

s = config.simulation.Waveform(
    mesh=salvus_mesh, sources=source
)

s.add_receivers(receivers, cpu_count=4)

# Define start time, end time, and time step:

# +
# Leave start time and time step as their auto-detected defaults.
s.physics.wave_equation.start_time_in_seconds = 0.0
s.physics.wave_equation.time_step_in_seconds = 4e-4

# Set end time.
s.physics.wave_equation.end_time_in_seconds = 2.0
# -

# Define a Dirichlet boundary at the top surface:

# +
# Define Dirichlet boundary at top surface (pressure-free).
dirichlet = config.boundary.HomogeneousDirichlet(side_sets=["y1"])

# Define coupled Clayton-Enqist / Kosloff
# damping boundaries at the the remaining side-s`ets.
absorbing = config.boundary.Absorbing(
    width_in_meters=200.0, #685.333,
    side_sets=["x0", "y0", "x1"],
    taper_amplitude=wavelet.center_frequency,
)

# Add the boundaries to the parameter file.
s.physics.wave_equation.boundaries = [absorbing, dirichlet]
# -

absorbing.width_in_meters

s.output.point_data.format = "hdf5"

# +
# s.output.volume_data.format = "hdf5"
# s.output.volume_data.filename = "output.h5"
# s.output.volume_data.fields = ["phi", "gradient-of-phi"]
# s.output.volume_data.sampling_interval_in_time_steps = 100
# -

# Initialize frequency domain (DFT) output:

# Initialize frequency domain output
s.output.frequency_domain.filename = "frequency_domain.h5"
s.output.frequency_domain.format = "hdf5"
s.output.frequency_domain.fields = ["phi"]
s.output.frequency_domain.frequencies = [5.0, 10.0, 15.0, 20.0]

# Validate the simulation:

s.validate()

# Run the simulation:

pprint(s.get_dictionary())

import pickle
with open('salvus_from_salvus.pkl', 'wb') as f:
    task_from_pickle = pickle.dump(s.get_dictionary(), f)

api.run(
    input_file=s,
    ranks=8,
    site_name="aug04",
    get_all=True,
    output_folder="output",
    overwrite=True,
)

# +
# Generate a shotgather from and HDF5 receiver file.
data, dt, extent = toolbox.get_shotgather("output/receivers.h5", cmp=1, field="gradient-of-phi")
print(f"dt is {dt}s")
print(data.shape)

# Normalize and plot the shotgather.
clip_min = -0.02 * data.max()
clip_max = +0.02 * data.max()
f = plt.figure(figsize=(15, 5))
plt.imshow(data, vmin=clip_min, vmax=clip_max, extent=extent, aspect="auto", cmap=plt.cm.seismic)

plt.xlabel("Offset (m)")
plt.ylabel("Time (s)")
plt.show()
# +
from pathlib import Path
from typing import List, Optional, Tuple, Union

import h5py
import matplotlib.tri as tri


# -

def visualize_dft_2d(
    filename: Union[str, Path], field: str, cmp: int = 0
) -> Tuple[tri.triangulation.Triangulation, np.ndarray]:
    """
    Get an object that can be used to plot simple 2-D wavefields.

    This method works by using the tricontourf method in matplotlib, and so
    comes with some limitations. For instance, if the mesh is not convex, the
    triangulation will fill in any holes and thus lead to unexpected results.
    This function is meant to quickly plot wavefields on simple domains for
    use in jupyter-notebooks and the like -- for more detailed volumetric
    plotting (including in 3-D) please use Paraview.

    Args:
        filename: Name of volumetric output file.
        field: Field to plot.
        cmp: Component of the field to plot.

    Returns:
        A tuple of a triangulation object, followed by the data which can be
        plotted with respect to that object, for instance plt.tricontourf(t,
        d[ts, :]) will plot the output at time-step ts.

    Raises:
        ValueError: If a field is completely unknown in Salvus.
        ValueError: If a field is not present in the file.
        ValueError: If a field is of a different physics than that in the file.
    """
    sf = set(("phi", "phi_t", "phi_tt", "gradient-of-phi"))
    vf = set(
        (
            "displacement",
            "velocity",
            "acceleration",
            "gradient-of-displacement",
            "strain",
        )
    )

    if field not in sf and field not in vf:
        raise ValueError("Unknown field specified.")
    physics = "scalar" if field in sf else "vector"

    crd_base = "/coordinates_{}"
    coord_names = {
        "scalar": crd_base.format("ACOUSTIC"),
        "vector": crd_base.format("ELASTIC"),
    }

    with h5py.File(str(filename), "r") as fh:

        # Read coordinates.
        if coord_names[physics] in fh:
            crds = fh[coord_names[physics]][:]
        else:
            raise ValueError(
                f"Request to plot a {physics} field, but no {physics} fields "
                f"were found in {filename}."
            )

        # Generate triangulation.
        npts, d = np.product(crds.shape[:2]), crds.shape[2]
        triang = tri.Triangulation(
            *np.squeeze(np.hsplit(crds.reshape(npts, d), d))
        )

        v_field = f"/frequency-domain/{field}"
        if v_field in fh:
            return (
                triang,
                (np.squeeze(fh[v_field][:, :, 1, :25])).reshape(-1, npts),
            )
        else:
            raise ValueError(
                f"Request to plot field {field}, but it was not found in "
                f"{filename}."
            )


tri2, data = visualize_dft_2d('output/frequency_domain.h5', 'phi')

data.shape

clip=1e2
plt.tricontourf(tri2, data[2, :].real, cmap=plt.cm.seismic, vmin=-clip, vmax=clip)

tri2, data = toolbox.visualize_wavefield_2d('output/wavefield.h5', 'phi')

data.shape

clip=1e2
plt.tricontourf(tri2, data[20, :], cmap=plt.cm.seismic, vmin=-clip, vmax=clip)

field = 'phi'
fh = h5py.File('output/wavefield.h5', "r")
v_field = f"/volume/{field}"
data = fh[v_field]
print(data.shape)

data[20, 1000, 0, 12]

coords = fh['coordinates_ACOUSTIC'][:]

coords.shape

coords[0, :]

plt.scatter(coords[3000, :, 0], coords[3000, :, 1])

51090*16

conn = fh['connectivity_ACOUSTIC']

conn.shape

conn[817439, :]

from scipy.interpolate import griddata
from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator

data = fh[v_field][:, :, :, :25]
# data.shape = 51, 
print(data.shape)
data2 = data[:, :, 0, :]
data2.shape = (51, -1)
print(data2.shape)

coords = fh['coordinates_ACOUSTIC'][:]
coords.shape = (-1, 2)
print(coords.shape)

fig = plt.figure(figsize=[15, 10])
ax = fig.subplots(nrows=1, ncols=1, squeeze=False)
ax[0, 0].scatter(coords[:, 0], coords[:, 1], c=data2[30, :], s=1)

linInter = LinearNDInterpolator(coords[:, :], data2[40, :])

nearInter = NearestNDInterpolator(coords[:, :], data2[40, :])

x_min, x_max, dx = 0, 2301*4, 4
y_min, y_max, dy = 0, 751*4, 4

grid_x, grid_y = np.mgrid[x_min:x_max:dx, y_min:y_max:dy]

grid_x.shape

values_interp = linInter(grid_x, grid_y)

values_interp = nearInter(grid_x, grid_y)

values_interp.shape

fig = plt.figure(figsize=[10, 7])
ax = fig.subplots(nrows=1, ncols=1, squeeze=False)
ax[0, 0].scatter(grid_x, grid_y, c=values_interp, s=1)

methods = [None, 'none', 'nearest', 'bilinear', 'bicubic', 'spline16',
           'spline36', 'hanning', 'hamming', 'hermite', 'kaiser', 'quadric',
           'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc', 'lanczos']

fig = plt.figure(figsize=[15, 10])
ax = fig.subplots(nrows=1, ncols=1, squeeze=False)
clip = 5e1
vmin, vmax = -clip, clip
ax[0, 0].imshow(values_interp.T, vmin=vmin, vmax=vmax, cmap=plt.cm.seismic, interpolation=methods[1])
ax[0, 0].invert_yaxis()
ax[0, 0].set_xlim(400, 600)
ax[0, 0].set_ylim(500, )



fh.close()
