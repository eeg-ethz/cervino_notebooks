# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# # Comparison: Matterhorn and Salvus

# %load_ext autoreload
# %autoreload 2

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt

import xarray as xr

# %matplotlib inline

# ## Conversion dictionaries

clip_time_dict = {
    'sxx': 5e+3,
    'vx': 5e-4,
    'vz': 5e-4,
}

clip_dft_dict = {
    'sxx': 1e+6,
}

rho = xr.load_dataset('model/marmousi_original.h5', engine="h5netcdf")['RHO']


def coeff(rho, x, z) -> dict:
    """Coefficient
    
    Each trace needs to be multiplied by the inverse of the density
    at the receiver location.
    
    Parameters
    ---------
    rho: xarray.Dataset
        Xarray Dataset of the density.
    x: float
        X-location.
    z: float
        Z-location.
    """
    rho0 = rho[rho.z==z, rho.x==x].values[0][0]
    return {
        'sxx': 1.0,
        'vx': -1.0 / rho0,
        'vz': 1.0/ rho0,
    }


# ## Load cervino output files for gathers and time slices

dsm = xr.load_dataset(f"output_mh/job_0000/output_0000.h5", engine="h5netcdf")

dss = xr.load_dataset(f"output_salvus/job_0000/output_0000.h5", engine="h5netcdf")

dsm.data_vars

dss.data_vars

# ## Gathers

fields_valid = {field: i for i, field in enumerate(dsm.coords['fields_gather'].values)}
pprint(fields_valid)

dam = dsm['gather_0000']
das = dss['gather_0000']

field_string = 'vx'
field = fields_valid[field_string]
c = coeff(rho, 20, 2000.0)[field_string]

clip = clip_time_dict[field_string]
vmin, vmax = -clip, clip

_ = dam[field, :, :].plot(aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = (c * das[field, :, :]).plot(aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

t = dsm['time_gather']
recx = dsm['recx']

# +
ir = 1500
# print(f"The receiver is at x: {recx[ir].values}")

# Figure
f, ax = plt.subplots(nrows=3, ncols=1, figsize=(12, 12), squeeze=False)

lims = [0.75, 1.9]
# lims = [1.6, 1.9]

field_string = 'vx'
field = fields_valid[field_string]
c = coeff(rho, ir * 4.0, 2000.0)[field_string]
print(1/c)

# Matterhorn
_ = ax[0, 0].plot(t, dam[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
_ = ax[0, 0].set_xlim(lims)

# Salvus
_ = ax[0, 0].plot(t, c * das[field, :, ir], label=f"Salvus {field_string}", marker='.')
_ = ax[0, 0].legend()
_ = ax[0, 0].set_xlim(lims)

field_string = 'vz'
field = fields_valid[field_string]
c = coeff(rho, ir * 4.0, 2000.0)[field_string]

# Matterhorn
_ = ax[1, 0].plot(t, dam[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[1, 0].legend()
_ = ax[1, 0].set_xlim(lims)

# Salvus
_ = ax[1, 0].plot(t, c * das[field, :, ir], label=f"Salvus {field_string}", marker='.')
_ = ax[1, 0].legend()
_ = ax[1, 0].set_xlim(lims)

field_string = 'sxx'
field = fields_valid[field_string]
c = coeff(rho, ir * 4.0, 2000.0)[field_string]

# Matterhorn
_ = ax[2, 0].plot(t, dam[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[2, 0].legend()
_ = ax[2, 0].set_xlim(lims)

# Salvus
_ = ax[2, 0].plot(t, c * das[field, :, ir], label=f"Salvus {field_string}", marker='.')
_ = ax[2, 0].legend()
_ = ax[2, 0].set_xlim(lims)
# -

# ## DFT wavefields

fields_valid = {field: i for i, field in enumerate(dsm.coords['fields_dft_slice'].values)}
pprint(fields_valid)

dam = dsm['slice_dft_0000']
das = dss['slice_dft_0000']

print(dam.shape)
print(das.shape)

ifreq = 4

freqs = dam['freq_dft'].values

field_string = 'sxx'
field = fields_valid[field_string]

clip = clip_dft_dict[field_string]
vmin, vmax = -clip, clip

_ = dam[field, ifreq, :, dam.x>30].real.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = das[field, ifreq, dam.x>30, :].real.T.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=True, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = dam[field, ifreq, :, dam.x>30].imag.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = das[field, ifreq, dam.x>30, :].imag.T.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=True, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

x = dsm['x']
z = dsm['z']

# +
ix = 650

# Figure
f, ax = plt.subplots(nrows=2, ncols=1, figsize=(12, 10), squeeze=False)

lims = [1000, 2500]

field_string = 'sxx'
field = fields_valid[field_string]

# Matterhorn
_ = ax[0, 0].plot(z, dam[field, ifreq, :, ix].real, label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
# _ = ax[0, 0].set_xlim(lims)

# Salvus
_ = ax[0, 0].plot(z, das[field, ifreq, ix, ::-1].real, label=f"Salvus {field_string}", marker='.')
_ = ax[0, 0].legend()
# _ = ax[0, 0].set_xlim(lims)

# Matterhorn
_ = ax[1, 0].plot(z, dam[field, ifreq, :, ix].imag, label=f"Matterhorn {field_string}")
_ = ax[1, 0].legend()
# _ = ax[0, 0].set_xlim(lims)

# Salvus
_ = ax[1, 0].plot(z, das[field, ifreq, ix, ::-1].imag, label=f"Salvus {field_string}", marker='.')
_ = ax[1, 0].legend()
# _ = ax[0, 0].set_xlim(lims)
