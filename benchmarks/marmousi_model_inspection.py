# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# # Marmousi model

# %matplotlib inline
import xarray as xr
import numpy as np

fn_h5 = 'model/marmousi_original.h5'

model = xr.load_dataset(fn_h5, engine="h5netcdf")

model.data_vars

model.coords

rho = model['RHO']
vp = model['VP']

rho[rho.z==2000, rho.x==1500*4]
