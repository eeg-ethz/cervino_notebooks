# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# # Save Salvus simple_config dictionary to file

import pickle
import toml
from pprint import pprint

with open('salvus_0000.pkl', 'rb') as f:
    salvus = pickle.load(f)

pprint(salvus)

with open('salvus_0000.toml', 'w') as f:
    toml.dump(salvus, f)
