# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %matplotlib inline

# +
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt

import wget
import shutil
from pathlib import Path
import h5netcdf

import segyio
# -

print(xr.__version__)

# !pwd

a = dict()
print(type(a))

dir_base = Path('/work/bfilippo/cervino/notebooks/marmousi_original_model')

# Template based on downloaded model folder.
template = "{}.segy"
par_map = {
    "VP": "velocity",
    "RHO": "density",
}

parameters = ['VP', 'RHO']

# Read raw data in using Obspy.
# We use decimation here to speed up the process.
raw_data = dict()
decimate = 10
for par in parameters:

    # Ensure that the file actually exists!
    fn = dir_base / template.format(par_map[par])
    assert fn.exists(), f"Filename {fn} does not exist!"

    # Read via Obspy into a numpy array.
    with segyio.open(fn, mode = 'r+', ignore_geometry=True) as f:
        f.bin[segyio.su.format] = 5
        raw_data[par] = f.trace.raw[:].T

nx, nz = raw_data['VP'].shape

spacing = 4

# Initialize the dataset with some coordiante arrays.
# Note here we are reversing the y-direction, as we want the origin
# (0, 0) to be in the bottom left corner of the resulting model.
data = xr.Dataset(
    coords={
        "x": np.arange(nx) * spacing,
        "z": np.arange(nz) * spacing,
    }
)

# Copy the data into xarray's data structure.
for item, array in raw_data.items():
    data[item] = (("x", "z"), array)

data['VP'].data.shape

data['VP'].data.astype('float32')

# **tofile()**
#
# Data is always written in ‘C’ order, independent of the order of a. The data produced by this method can be recovered using the function fromfile().

data2 = data.transpose('z', 'x')
data2

data2['VP'].data.shape

data2['VP'].data.flags

data['VP'].data.astype('float32').tofile('prova_x.bin')

a_x = np.fromfile('prova_x.bin', dtype=np.float32)
a_c = np.fromfile('prova_c.bin', dtype=np.float32)

plt.plot(a_x[:750])
plt.plot(a_c[:750], marker='x')
plt.plot(data['VP'].data[0, :750], marker='o')

data

data['VP'][3][0].data.__array_interface__['data']

plt.imshow(data['VP'].data)

data['RHO'].plot(aspect="auto", figsize=(10, 3), yincrease=False)

fn = 'marmousi.netcdf'
fn = 'marmousi_original_model/marmousi_original.h5'

data.to_netcdf(fn, format='NETCDF4')

import h5py

with h5py.File(fn, 'r') as f:
    print(f.keys())
    plt.imshow(f['VP'][:].T, cmap=plt.cm.seismic)

ds = xr.Dataset({'foo': (('x', 'y', 'z'), [[[42]]]), 'bar': (('y', 'z'), [[24]])})
ds['foo'].data.flags

ds.transpose('y', 'z', 'x')

# ## From the official documentation:

import pandas as pd

# +
temp = 15 + 8 * np.random.randn(2, 2, 3)

precip = 10 * np.random.rand(2, 2, 3)

lon = [[-99.83, -99.32], [-99.79, -99.23]]

lat = [[42.25, 42.21], [42.63, 42.59]]

# for real use cases, its good practice to supply array attributes such as
# units, but we won't bother here for the sake of brevity
data_vars = {
    'temperature': (['x', 'y', 'time'],  temp),
    'precipitation': (['x', 'y', 'time'], precip)
}
coords = {
    'lon': (['x', 'y'], lon),
    'lat': (['x', 'y'], lat),
    'time': pd.date_range('2014-09-06', periods=3),
    'reference_time': pd.Timestamp('2014-09-05')
}

ds = xr.Dataset(data_vars, coords)
# -

ds.to_netcdf('ds.h5')

ds['precipitation'].shape

# +
# Number of receivers
nr = 20
# Number of time steps
nt = 15

# Data (fake)
gather = 10 * np.random.rand(nr, nt)

# Receivers as trace numbers
r = np.arange(nr, dtype=np.int)

# Coordinates of receivers(x, y) (fake)
x = 10 * np.random.rand(nr)
y = 10 * np.random.rand(nr)
loc = 10 * np.random.rand(nr, 2)

# Time axis (fake)
time = np.arange(nt) * 0.1

data_vars = {
    'gather': (['r', 'time'],  gather),
}
coords = {
    'r': r,
    'loc': (['x', 'y'], loc),
    'time': time
}

ds = xr.Dataset(data_vars, coords)

# +
# ds.to_netcdf('gather_prova.h5', format='NETCDF4')
# -

time > 1.0

ds.dims

ds['gather'].loc[:, time > 0]

ds['gather'][:, time > 1]

# ## Example from Dieter
# From a [thread](https://swung.slack.com/archives/C094K6WR5/p1577165391006200) on Slack

import xarray as xr
n_sources = 3
n_receivers = 5
n_freqs = 4
x_src = xr.DataArray(np.linspace(-3, 3, n_sources), dims=['ns'], name='x_src')
y_src = xr.DataArray(np.linspace(-4, 4, n_sources), dims=['ns'], name='y_src')
z_src = xr.DataArray(np.linspace(-5, 5, n_sources), dims=['ns'], name='z_src')
x_rcv = xr.DataArray(np.linspace(-7, 7, n_receivers), dims=['nr'], name='x_rcv')
y_rcv = xr.DataArray(np.linspace(-8, 8, n_receivers), dims=['nr'], name='y_rcv')
z_rcv = xr.DataArray(np.linspace(-9, 9, n_receivers), dims=['nr'], name='z_rcv')
freqs = xr.DataArray(np.linspace(100, 1000, n_freqs), dims=['nf'], name='freqs')
data = xr.DataArray(np.random.rand(n_sources, n_receivers, n_freqs), dims=['ns', 'nr', 'nf'],
                    coords=dict(x_src=x_src, y_src=y_src, z_src=z_src, x_rcv=x_rcv, y_rcv=y_rcv, z_rcv=z_rcv), name='data')
ds = xr.Dataset({'myData':data})

ds['myData'].coords

# +
src = np.random.rand(2, 9)
rec = np.random.rand(2, 9)

# freqs = np.random.rand(5)

srcx = xr.DataArray(src[0], dims=['src_name'])
srcy = xr.DataArray(src[1], dims=['src_name'])
recx = xr.DataArray(rec[0], dims=['rec_name'])
recy = xr.DataArray(rec[1], dims=['rec_name'])

freqs = xr.DataArray(np.arange(10), dims=['frequency'])

src_names = ['Tx001', 'Tx002', 'Tx003', 'Tx004', 'Tx005', 'Tx006', 'Tx007', 'Tx008', 'Tx009']
rec_names = [f"Rx{a:003.0f}" for a in np.arange(1, rec[0].size+1)]

# Initiate data with zeros
data = xr.DataArray(
    data=np.zeros((src[0].size, rec[0].size, 6, freqs.size), dtype=complex),  # dummy data
    dims=['src_name', 'rec_name', 'component', 'frequency'],
    coords=dict(srcx=srcx,
                srcy=srcy,
                recx=recx,
                recy=recy,
                src_name=src_names,
                rec_name=rec_names,
                component=["ex", "ey", "ez", "hx", "hy", "hz"],
                frequency=freqs),
    name='data'
)
# -

data.coords

data.loc['Tx001', 'Rx002', 'ey', 5]

data.loc['Tx001', recx < 0.6, 'ey', 5]


