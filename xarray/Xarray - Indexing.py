# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

import numpy as np
import xarray as xr

# +
src = np.random.rand(2, 9)
rec = np.random.rand(2, 9)

# srcx = xr.DataArray(src[0], dims=['src_name'])
# srcy = xr.DataArray(src[1], dims=['src_name'])
# recx = xr.DataArray(rec[0], dims=['rec_name'])
# recy = xr.DataArray(rec[1], dims=['rec_name'])

freqs = np.arange(10)
# freqs = xr.DataArray(np.arange(10), dims=['frequency'])

src_names = ['Tx001', 'Tx002', 'Tx003', 'Tx004', 'Tx005', 'Tx006', 'Tx007', 'Tx008', 'Tx009']
rec_names = [f"Rx{a:003.0f}" for a in np.arange(1, rec[0].size+1)]
# -

# Initiate data with zeros
data_a = xr.DataArray(
    data=np.zeros((src[0].size, rec[0].size, 6, freqs.size), dtype=np.float32),  # dummy data
    dims=['src_name', 'rec_name', 'component', 'frequency'],
    coords=dict(srcx=srcx,
                srcy=srcy,
                recx=recx,
                recy=recy,
                src_name=src_names,
                rec_name=rec_names,
                component=["ex", "ey", "ez", "hx", "hy", "hz"],
                frequency=freqs),
    name='data'
)
# dsa = data_a.to_dataset(name="data_a")
# dsa.to_netcdf('dsa.h5')

dsa_from_file = xr.load_dataset('dsa.h5')

dsa_from_file.coords

da = dsa_from_file['data_a']
da.loc['Tx001', da.recx < 0.6, 'ey', 5].recx

import pandas as pd
data = np.random.rand(4, 3)
locs = ['IA', 'IL', 'IN']
times = pd.date_range('2000-01-01', periods=4)
da_doc = xr.DataArray(
    data,
    coords={
        'time': times, 'space': locs, 'const': 42,'ranking': ('space', [1, 2, 3]),
    },
    dims=['time', 'space']
)
da_doc

src = np.random.rand(2, 9)
rec = np.random.rand(2, 9)
freqs = np.arange(10)
src_names = ['Tx001', 'Tx002', 'Tx003', 'Tx004', 'Tx005', 'Tx006', 'Tx007', 'Tx008', 'Tx009']
rec_names = [f"Rx{a:003.0f}" for a in np.arange(1, rec[0].size+1)]
# Initiate data with zeros
data_a = xr.DataArray(
    data=np.zeros((src[0].size, rec[0].size, 6, freqs.size), dtype=np.float32),  # dummy data
    coords={
        'src_name': src_names,
        'rec_name': rec_names,
        'component': ["ex", "ey", "ez", "hx", "hy", "hz"],
        'frequency': freqs,
        'srcx': ('src_name', src[0]),
        'srcy': ('src_name', src[1]),
        'recx': ('rec_name', rec[0]),
        'recy': ('rec_name', rec[1]),
    },
    dims=['src_name', 'rec_name', 'component', 'frequency'],
    name='data'
)
dsa = data_a.to_dataset(name="data_a")
dsa.to_netcdf('dsa.h5')

data_b = xr.DataArray(
    data=np.zeros((src[0].size, rec[0].size, 6, freqs.size), dtype=np.float32),  # dummy data
    coords={
        'src_name': src_names,
        'rec_name': rec_names,
        'component': ["ex", "ey", "ez", "hx", "hy", "hz"],
        'frequency': freqs,
        'srcx': ('src_name', src[0]),
        'srcy': ('src_name', src[1]),
        'recx': ('rec_name', rec[0]),
        'recyyyy': ('rec_name', rec[1]),
    },
    dims=['src_name', 'rec_name', 'component', 'frequency'],
    name='data'
)

dsa['data_b'] = data_b

dsa.coords

dsa.coords

# Initiate data with zeros
data_b = xr.DataArray(
    data=np.zeros((src[0].size, rec[0].size, 6, freqs.size), dtype=complex),  # dummy data
    dims=['src_name', 'rec_name', 'component', 'frequency'],
    coords=dict(src_name=src_names,
                rec_name=rec_names,
                component=["ex", "ey", "ez", "hx", "hy", "hz"],
                frequency=freqs),
    name='data'
)
data_b.coords

data_a.

data_a.loc['Tx001', recx < 0.6, 'ey', 5]

data_b.loc['Tx001', recx < 0.6, 'ey', 5]
