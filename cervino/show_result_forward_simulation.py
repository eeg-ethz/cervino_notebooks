# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: show results of forward simulation</h1>
# </div>

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

import matplotlib.pyplot as plt

from pprint import pprint

import xarray as xr



import numpy as np
import h5py
from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator

x_min, x_max = 0, 9200
z_min, z_max = 0, 3000
grid_x, grid_z = np.mgrid[x_min:x_max:2301j, z_min:z_max:751j]
print(grid_x.shape)
print(grid_z.shape)

# ```
# DATASET "coordinates_ACOUSTIC" {
#     DATATYPE  H5T_IEEE_F64LE
#     DATASPACE  SIMPLE { ( 16720, 25, 2 ) / ( 16720, 25, 2 ) }
# ```

# ```
# DATASET "FFT-img-10.000000-Hz" {
#     DATATYPE  H5T_IEEE_F32LE
#     DATASPACE  SIMPLE { ( 2, 16720, 1, 32 ) / ( 2, 16720, 1, 32 ) }
# ```

#

dims = 2
with h5py.File('output/job_0000/slice_dft.h5', 'r') as f:
    coords = f['coordinates_ACOUSTIC'][:, :, :].reshape((-1, dims))
    das= f['volume']['FFT-img-20.000000-Hz'][:, :, 0, :25]
#     das = das.reshape((das.shape[0], -1))
    das = das[-1, ...].reshape((-1))
    print(das.shape)
    print(coords.shape)
    interpolator = LinearNDInterpolator(coords[:, :], das[:])
    data = interpolator(grid_x, grid_z)
    print(data.shape)

data.shape

plt.figure(figsize=[15, 5])
plt.imshow(data[:, :].T)
plt.colorbar()





# <div class="alert alert-info">
#     <h2>Show results</h2>
# </div>

# First, we load one of the HDF5 output file as an `xarray.Dataset`:

ds = xr.load_dataset('output/job_0000/output_0000.h5', engine='h5netcdf')

# We now list the `xarray.DataArray`s included in the dataset:

ds.data_vars

# Print the dimensions and shape of each dataset:

for da in ds:
    print(da, ds[da].dims, ds[da].shape)

# We show the coordinates associated with the datasets:

ds.coords

ds.coords['time_slice'].shape

# Print the dimensions and shape of each coordinate:

for coord in ds.coords:
    print(coord, ds.coords[coord].dims, ds.coords[coord].shape)

# Now we plot a time slice:

da = ds['slice_time_0000']

clip = 1e0
vmin, vmax = -clip, clip
_ = da[0, 9, da.x>300, :].T.plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# Now we plot a DFT slice:

da = ds['slice_dft_0000']

clip = 1e-2
vmin, vmax = -clip, clip
_ = da[0, da.freq_dft==20, :, da.x>400].real.plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# Finally, we plot a shot gather:

da = ds['gather_0000']

clip = 1e-3
vmin, vmax = -clip, clip
_ = da[2, da.time_gather>0.1, da.recx>1000].plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)
