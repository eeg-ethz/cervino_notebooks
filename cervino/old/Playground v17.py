# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: run simulations with Matterhorn</h1>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

# <div class="alert alert-info">
#     <h2>Import packages</h2>
# </div>

from collections import namedtuple

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
import segyio

# +
# Set numerical precision
# DTYPE = np.float64
# -

# Imports for Cervino.
from cervino import *

# <div class="alert alert-info">
#     <h2>Cervino setup</h2>
# </div>

# General Cervino class which allows one to do most of the work. This class has methods to create the TOML file, the solver input file, to run the simulation, ...

workflow_type = 'forward'

workflow_dict = create_workflow_dict(workflow_type)

workflow_dict['compute_forward_simulation'] = SolverSite('matterhorn', 'local')

cervino_dict = {
    'workflow_type': workflow_type,
    'workflow_dict': workflow_dict,
    'name': 'marmousi_v15',
    'site': 'aug04',
    'desc': 'Simple Matterhorn forward simulation on the Marmousi model',
}

pprint(cervino_dict)

marmousi = Cervino(**cervino_dict)

marmousi

# Construct the dictionaries for the tasks that will compose the forward simulation pipe. This is done in another Notebook. Here, we load the tasks into the pipeline of the Cervino simulation:

marmousi.workflow.pipeline['compute_forward_simulation'].attach_tasks_from_pickle('tasks.pkl')

summary = marmousi.summary()
for line in summary:
    print(line)

# +
# pprint(marmousi.workflow.pipeline['compute_forward_simulation'].get_tasks_dict())['0001'])
# -

# <div class="alert alert-info">
#     <h2>Validate simulation</h2>
# </div>

# Before running the simulation, we want to validate its input to make sure it is correct:

# +
# marmousi.validate()
# -

# <div class="alert alert-info">
#     <h2>Run simulation</h2>
# </div>

# <div class="alert alert-info">
#     <h3>Matterhorn (local)</h3>
# </div>

# This launches the simulation on the local workstation/laptop.

# We first need to define a small dictionary with a few required parameters:

run_dict = {
    'progress': True,
    'verbose': True,
    'n_ranks': 8,
    'max_ranks': 16
}

# +
# for a in marmousi.workflow.pipeline['compute_forward_simulation'].get_tasks_dict().values():
#     pprint(a)
# -

result, output_files_hdf5 = marmousi.run(run_dict)

output_files_hdf5

# +
# for i in result['compute_forward_simulation']:
#     print(i[0].decode("utf-8") )

# +
# marmousi.get_output_files(verbose=False)

# +
# task = marmousi.workflow.pipeline['compute_forward_simulation']._tasks_dict['0002']
# pprint(task)
# -

ofd = Tasks._get_output_files_from_task(marmousi.workflow.pipeline['compute_forward_simulation']._tasks_dict['0002'])
pprint(ofd)

import xarray as xr

ds = xr.load_dataset('0001.h5')

ds.data_vars

ds.dims

# da = ds['gather_0000']
da = ds['slice_time_0000']

clip = 1e-9
vmin, vmax = -clip, clip
da[da.time_slice==0.8, da.x>3000, :].T.plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

da = ds['slice_dft_0000']

da.coords

da.dims

clip = 1e-2
vmin, vmax = -clip, clip
da[da.freq_dft==20, :, da.x>400].plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# <div class="alert alert-info">
#     <h2>Show results</h2>
# </div>

# <div class="alert alert-info">
#     <h3>Matterhorn (local)</h3>
# </div>

filename = 'vz_gather.su'
# with segyio.open(filename, ignore_geometry=True) as segyfile:
sufile = segyio.su.open(filename, ignore_geometry=True, endian='little')
sufile.mmap()

# +
clip = 1e-9
vmin, vmax = -clip, clip

# Figure
figsize=(10, 15)
ndim = 1
fig, axs = plt.subplots(nrows=1, ncols=ndim, figsize=figsize, facecolor='w', edgecolor='k', squeeze=False)

axs = axs.ravel()

axs[0].imshow(sufile.trace.raw[:].T, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax, aspect=1)
_ = axs[0].set_xlabel('Horizontal location')
_ = axs[0].set_ylabel('Time [s]')

# plt.savefig('gather.png', dpi=300, bbox_inches='tight')
# -

sufile.close()

# <div class="alert alert-info">
#     <h2>Convert output to HDF5</h2>
# </div>

import h5py

from pathlib import Path

fn = f'{marmousi.name}.h5'
print(fn)

marmousi.convert_output_to_hdf5()

outputs = marmousi.get_output_files()
pprint(outputs)

for output_k, output_v in outputs.items():
    print(output_v[0])
    print(output_v[1])

with h5py.File(fn, 'r') as f:
    print(type(f.attrs['groups']))
    print(f.attrs['groups'].dtype)
    print(f.attrs['groups'])
    print(type(f['slice_dft'].attrs['dft_frequencies']))
    b = f['slice_dft'].attrs['datasets']
    print(type(b))
    print(b.dtype)
    print(b)
    a = np.array(['ciao', 'ciaone'])
    print(a)
    print(a.dtype)
#     f['slice_dft'].attrs['datasets'] = a

# print(fn)
with h5py.File(fn, 'r') as f:
    for item in f.keys():
        print(list(f[item].attrs['datasets']))
#         print(type(item))

with h5py.File(fn, 'r') as f:
#     for item in f.attrs.keys():
#         print(item)
    print(f.attrs['groups'])

with h5py.File(fn, 'r') as f:
    pprint(f['slice_time'].attrs['datasets'])

print(fn)
with h5py.File(fn, 'r') as f:
    fig, ax = plt.subplots(nrows=2, ncols=1)
    ax[0].imshow(f['slice_dft']['sxx_dft_slice'][12].real, cmap=plt.cm.seismic)
    ax[1].imshow(f['slice_dft']['sxx_dft_slice'][12].imag, cmap=plt.cm.seismic)

print(fn)
with h5py.File(fn, 'r') as f:
    plt.imshow(f['slice_time']['vx_slice'][27].T, cmap=plt.cm.seismic)

with h5py.File('marmousi_original_model/marmousi_original.h5', 'r') as f:
    print(f['VP'].shape)


