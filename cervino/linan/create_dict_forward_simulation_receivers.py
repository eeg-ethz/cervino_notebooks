# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: create dict for forward simulation</h1>
# </div>

# This tutorial shows how to build the Python dictionary required to define a forward simulation. To do so, we will first write a function to create each section separately and then we will build the final dictionary combining all the functions.

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
import pickle

# Imports for Cervino.
from cervino.core import get_general_dict, get_time_dict, get_frequency_dict, get_physics_dict, get_pml_dict
from cervino.utils.models import create_xarray_from_binary
from cervino.domain import AcousticDomain
from cervino.input.wavelets import RickerWavelet, UserWavelet
from cervino.input.sources import PointSource    
from cervino.output.receivers import Gather
from cervino.output.slices import TimeSlice, DFTSlice

# <div class="alert alert-info">
#     <h2>Cervino dictionary sections</h2>
# </div>

# 1. [General](#general-section)
# 2. [Domain](#domain-section)
# 3. [Time and frequency](#time-frequency-section)
# 4. [Source](#source-section)
# 5. [Output](#output-section)
# 6. [Physics](#physics-section)
# 7. [Assemble the `cervino` dictionary](#assembly-section)
# 8. [Define source locations](#source-locations)
# 9. [Create the `cervino` dictionary](#create-dictionary)
# 10. [Export the `cervino` dictionary](#export-dictionary)

# <div id='general-section' class="alert alert-info">
#     <h3>General section</h3>
# </div>
# <a ></a>

# In this initial section, we define a few general parameters required by the simulation. Since we want to perform a **forward simulation**, we set `task_type` equal to `compute_forward_simulation`. First, we select the specific solver for this simulation.

task_type = 'compute_forward_simulation'

solver = 'matterhorn'


def cervino_general_dict(uid: str):
    return get_general_dict(
        uid=uid,
        task_type=task_type,
        solver=solver,
        desc=f"{solver} and marmousi"
    )


# <div id='domain-section' class="alert alert-info">
#     <h3>Domain section</h3>
# </div>

# In this section, we need to define the domain used for the simulation: geometries and model parameters representing the specific portion of the subsurface that we want to study. In the example below, we choose the original Marmousi model. First thing, we need to obtain the binary files from the internet.

# +
nx = 128
ny = 1
nz = 256
n = (nx, ny, nz)

dx = dy = dz = 1.0
d = (dx, dy, dz)
# -

filename_prefix = 'model_{}.bin'

# +
# This is how you should create a model in MATLAB
# vp = ones(nz, nx, 'single');
# vp(150:end, :) = 2500;
# imagesc(vp)
# fid = fopen('model_vp_matlab', 'w', 'n');
# fwrite(fid, vp, 'single');
# fclose(fid);

# +
# This is how you create a model with NumPy
# vp = np.ones((nx, nz), dtype=np.float32) * 2000.0
# vp.tofile('model_VP.bin')
# rho = np.ones_like(vp) * 1000.0
# vp.tofile('model_RHO.bin')

# +
# vp = np.fromfile('model_vp_matlab', dtype=np.float32).reshape((nx, nz))
# print(vp.shape)
# plt.imshow(vp.T)
# -

# We then set the velocity and density models directly from the HDF5 created by the utility above:

# +
# We instantiate an AcousticDomain object:
domain = AcousticDomain()

# Create xarray from binary files
dataset, fn_h5 = create_xarray_from_binary(filename_prefix, domain.required_models, n=n, d=d, solver='matterhorn')

# Set the models
domain.set_model_from_xarray(dataset, fn_h5)

def cervino_domain_dict():
    return domain.get_domain_dict()


# -

# <div id='time-frequency-section' class="alert alert-info">
#     <h3>Time and frequency sections</h3>
# </div>

# Here we need to define the time step in seconds `time_step_in_seconds` and the number of time steps `number_of_timesteps`. In the example below, we first define the time step, initial, and final time, and use these values to compute the number of time steps. However, one could also define the number of time steps directly.

# +
time_step_in_seconds = 3e-4
number_of_timesteps = 4000
start_time_in_seconds = 0.0
# time_shift_in_seconds = 1.0
end_time_in_seconds = (number_of_timesteps - 1) * time_step_in_seconds

# Time axis
t = np.arange(start_time_in_seconds, end_time_in_seconds, time_step_in_seconds)

def cervino_time_dict():
    return get_time_dict(ts=time_step_in_seconds, nt=number_of_timesteps, t0=start_time_in_seconds)


# -

# <div id='source-section' class="alert alert-info">
#     <h3>Source section</h3>
# </div>

# Here, we define the input source which will generate the wavefields. In this tutorial, we define a point source characterised by a Ricker wavelet as source time function.

# +
# Use a Ricker wavelet as source time function
center_frequency = 75.0
stf = RickerWavelet(t, fc=center_frequency, delay=1.5, delay_type='cycles', create_binary=True, fn_prefix='source')

# Additionally, we can also load a user-defined wavelet from a binary file.
# filename = 'user_defined_ricker_20hz.bin'
# stf = UserWavelet(filename, nt=t.size)

# Define a point source
name = "source0"
# spatial_type = 'q'
scale = 1.

def cervino_source_dict(uid: str, location: list, spatial_type: str):
    return PointSource(loc=location, stf=stf, spatial_type=spatial_type).get_source_dict()


# +
# We compute the FFT of the source time function.
# The values of the FFT at specific frequencies will be required later when defining DFT slice outputs.

# Frequency step in Hz
df = 1 / (time_step_in_seconds * number_of_timesteps)
Nfft = stf.wavelet.size

# Frequency axis
f = np.arange(Nfft) * df

# FFT
W = np.fft.fft(stf.wavelet)

# Plot
plt.plot(f, np.abs(W))
_ = plt.title('FFT of Ricker wavelet')
_ = plt.xlabel('Frequency [Hz]')
# _ = plt.xlim([0, 100])
# -

# <div id='output-section' class="alert alert-info">
#     <h3>Output section</h3>
# </div>

# Here, we define the output of the simulation. First, we create a shot gather:

def cervino_gather_dict():
    # First we create a general gather object
    start_timestep = 0
    end_timestep = len(t) - 1
    timestep_increment = 1

    gather = Gather(
        start_timestep=start_timestep,
        end_timestep=end_timestep,
        timestep_increment=timestep_increment,
        fmt='su'
    )

    # Number of receivers
    nr = 12
    # Receiver x-value
    rx0 = 114.0
    # z-value of first receiver
    rz0 = 14.0

    # Then we define multiple receiver arrays
    rec0 = gather.add_receivers(
        origin=(rx0, 0.0, rz0),
        increment=(0.0, 0.0, 20.0),
        nr=nr,
        fields=['sxx', 'vx', 'vz'],
        filename="gather0"
    )
    
    return gather.get_receivers_dict()


# Additionally, we create time snapshots:

def cervino_slice_time_dict():
    # First we create a general slice object
    start_timestep = 0
    end_timestep = len(t) - 1
    timestep_increment = 100

    snapshot = TimeSlice(
        start_timestep=start_timestep,
        end_timestep=end_timestep,
        timestep_increment=timestep_increment,
        fmt='su'
    )

    # Axis
    slice_axis = 'y'
    # Slice index
    slice_index = 0

    # Then we define two slices
    slice0 = snapshot.add_slices(
        axis=slice_axis, 
        slice_index=slice_index,
        fields=['vx', 'vz'],
        filename="slice_time"
    )
    
    return snapshot.get_slices_dict()


# Additionally, we create DFT slices:

def cervino_slice_dft_dict(gf: bool=True):
    # First we create a general snapshot object
    dft_frequencies = [25.0, 50.0, 75.0, 100.0, 125.0, 150.0]

    # Write wavelet coefficients to file
    dft_wavelet_coefficients = np.array([W[int(i / df)] for i in dft_frequencies], dtype=np.complex64)
    if gf:
        dft_wavelet_coefficients_fn = f"dft_wavelet_coefficients_source0.bin"
    else:
        dft_wavelet_coefficients = np.ones_like(dft_wavelet_coefficients)
        dft_wavelet_coefficients_fn = f"dft_wavelet_coefficients_source0_nogf.bin"
    dft_wavelet_coefficients.tofile(dft_wavelet_coefficients_fn)

    dft = DFTSlice(
        dft_frequencies=dft_frequencies,
        dft_wavelet_coefficients=dft_wavelet_coefficients_fn,
        fmt='su'
    )
    
    # Axis
    slice_axis = 'y'
    # Slice index
    slice_index = 0

    dft_slice0 = dft.add_slices(
        axis=slice_axis,
        slice_index=slice_index,
        fields=['sxx', 'vx', 'vz'],
        filename="slice_dft"
    )
    
    return dft.get_slices_dict()


# <div id='physics-section' class="alert alert-info">
#     <h3>Physics section</h3>
# </div>

# +
# We define the PML boundary conditions:
pml_dict = get_pml_dict(fs=False, width=10, power=4, frequency=center_frequency, vel=2000.)

# We define the order of the spatial stencil:
def cervino_physics_dict():
    return get_physics_dict(order=4, boundaries=[pml_dict,])


# -

# <div id='assembly-section' class="alert alert-info">
#     <h3>Assemble complete cervino dictionary</h3>
# </div>

# Generate the complete input dictionary.
def compute_forward_simulation_get_dict(uid, source_location, spatial_type):
    return {
        "general": cervino_general_dict(uid),
        "domain": cervino_domain_dict(),
        "time": cervino_time_dict(),
        "physics": cervino_physics_dict(),
        "source": {
            "point-source": cervino_source_dict(uid, source_location, spatial_type),
        },
        "output": {
#             "gather": cervino_gather_dict(),
#             "slice_time": cervino_slice_time_dict(),
            "slice_dft": cervino_slice_dft_dict(gf=True),
        }
    }


# <div id='source-locations' class="alert alert-info">
#     <h2>Define source locations - 2D line</h2>
# </div>

# We want to simulate multiple shots. Each simulation corresponds to a *task* within the sime *pipe*. In this tutorial, we are building the **compute_forward_simulation** pipe. This *pipe* could be the only *pipe* within a more complex *pipeline*. We now define the location of the shots:

locations = [(114.0, 0.0, z) for z in np.arange(14.0, 245.0, 20.0)]
print(f"Number of shot locations: {len(locations)}")
print("The locations are:")
pprint(locations)

# <div id='create-dictionary' class="alert alert-info">
#     <h2>Create final cervino dictionary</h2>
# </div>

# In this example, we want to compose the dictionary for one or more forward simulations: one simulation for each shot location. Each simulation corresponds to a *task* of a more complex *pipeline*. A *pipeline* is composed of *pipes*. In this specific tutorial, we are defining the *tasks* of the **compute_forward_simulation** *pipe*. Each *task* is identified by a *uid* (unique identifier). **The *uid* must be a string**.

uid = "{:04d}"
tasks = dict()
# Important, we set the initial value of `i` to 36 because the uids of the simulations
# created with the create_dict_forward_simulation_receivers.ipynb Notebook
# range from 0000 to 0035.
i = 36
for spatial_type in ['q', 'fx', 'fz']:
    for loc in locations:
        tasks[uid.format(i)] = compute_forward_simulation_get_dict(uid.format(i), loc, spatial_type)
        i += 1

# The dictionary `tasks` (created in the cell above) is a dictionary of dictionaries. They `keys` of `tasks` are the *uids* of the three simulations:

pprint(tasks.keys())

# The dictionary of the simulation corresponding to *uid* `0000` is:

pprint(tasks['0042'])

# The dictionary above **is** the required `cervino` input in dictionary format.

# <div id='export-dictionary' class="alert alert-info">
#     <h2>Export the cervino dictionary</h2>
# </div>

# A dictionary can be serialized and saved to a file using the [pickle](https://docs.python.org/3/library/pickle.html) module. This allows us to load the `tasks` dictionary from another Jupyter Notebook or Python script.

output_file = 'tasks_receivers.pkl'
with open(output_file, 'wb') as f:
    pickle.dump(tasks, f)

# To check that the dictionary was correctly exported, we load it again from file and print the same dictionary as above (this is not a necessary step!):

with open(output_file, 'rb') as f:
    tasks_from_pickle = pickle.load(f)

pprint(tasks_from_pickle['0000'])
