# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %load_ext autoreload
# %autoreload 2

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
import segyio
import h5py

from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator

import xarray as xr

from salvus_toolbox import toolbox

# %matplotlib inline

# # Conversion dictionaries

fields_valid = {
    'sxx': 0,
    'vx': 1,
    'vz': 2,
}

fields_valid = {
    'vx': 0,
    'vz': 1,
}

# # Load cervino output files

dsm = xr.load_dataset('../cervino/output/job_0000/output_0000.h5', engine="h5netcdf")

dss = xr.load_dataset('../salvus/output/job_0000/output_0000.h5', engine="h5netcdf")

dsm.data_vars

dss.data_vars

# # Gathers

dam = dsm['gather_0000']
das = dss['gather_0000']

field_string = 'sxx'
field = fields_valid[field_string]

clip_dict = {
    'sxx': 5e+3,
    'vx': 5e-4,
    'vz': 5e-4,
}

clip = clip_dict[field_string]
vmin, vmax = -clip, clip

coeff_vx = -1 if field_string == 'vx' else 1
print(coeff_vx)

_ = dam[field, :, :].plot(aspect="auto", figsize=(9, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = das[field, :, :].plot(aspect="auto", figsize=(9, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

t = dsm['time_gather']
recx = dsm['recx']

# +
ir = 1500
# print(f"The receiver is at x: {recx[ir].values}")

# Figure
f, ax = plt.subplots(nrows=2, ncols=2, figsize=(12, 12), squeeze=False)

# Matterhorn
_ = ax[0, 0].plot(t, dam[0, :, ir], label='Matterhorn sxx')
_ = ax[0, 0].legend()
_ = ax[0, 0].set_xlim([0.75, 1.5])

# Salvus
_ = ax[0, 1].plot(t, das[0, :, ir], label='Salvus sxx')
_ = ax[0, 1].legend()
_ = ax[0, 1].set_xlim([0.75, 1.5])

# Matterhorn
_ = ax[1, 0].plot(t, dam[1, :, ir], label='Matterhorn vx')
_ = ax[1, 0].plot(t, dam[2, :, ir], label='Matterhorn vz')
_ = ax[1, 0].legend()
_ = ax[1, 0].set_xlim([0.75, 1.5])

# Salvus
coeff = 1.0 / 2000.0
_ = ax[1, 1].plot(t, - coeff * das[1, :, ir], label='Salvus vx')
_ = ax[1, 1].plot(t, coeff * das[2, :, ir], label='Salvus vz')
_ = ax[1, 1].legend()
_ = ax[1, 1].set_xlim([0.75, 1.5])
# -

# # Time wavefields

rho0 = 2000.0

dam = dsm['slice_time_0000']
das = dss['slice_time_0000'] / rho0

it = 20

field_string = 'vz'
field = fields_valid[field_string]

clip_dict = {
    'sxx': 5e+3,
    'vx': 5e-4,
    'vz': 5e-4,
}

clip = clip_dict[field_string]
vmin, vmax = -clip, clip

coeff_vx = -1 if field_string == 'vx' else 1
print(coeff_vx)

_ = dam[field, it, dam.x>300, :].T.plot(add_colorbar=False, aspect="auto", figsize=(9, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = das[field, it, dam.x>300, :].T.plot(add_colorbar=False, aspect="auto", figsize=(9, 5), yincrease=True, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

x = dsm['x']
z = dsm['z']

field

# +
it = 20
ix = 1000

# Figure
f, ax = plt.subplots(nrows=2, ncols=2, figsize=(12, 12), squeeze=False)

# Matterhorn
_ = ax[0, 0].plot(z, dam[field, it, ix, :], label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
# _ = ax[0, 0].set_xlim([0.75, 1.5])

# Salvus
_ = ax[0, 1].plot(z, das[field, it, ix, ::-1], label=f"Salvus {field_string}")
_ = ax[0, 1].legend()
# _ = ax[0, 1].set_xlim([0.75, 1.5])

# # Matterhorn
# _ = ax[1, 0].plot(t, dam[1, :, ir], label='Matterhorn vx')
# _ = ax[1, 0].plot(t, dam[2, :, ir], label='Matterhorn vz')
# _ = ax[1, 0].legend()
# _ = ax[1, 0].set_xlim([0.75, 1.5])

# # Salvus
# coeff = 1.0 / 2000.0
# _ = ax[1, 1].plot(t, - coeff * das[1, :, ir], label='Salvus vx')
# _ = ax[1, 1].plot(t, coeff * das[2, :, ir], label='Salvus vz')
# _ = ax[1, 1].legend()
# _ = ax[1, 1].set_xlim([0.75, 1.5])
# -


