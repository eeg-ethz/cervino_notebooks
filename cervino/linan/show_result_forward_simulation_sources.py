# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: show results of forward simulation</h1>
# </div>

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from pprint import pprint

import matplotlib.pyplot as plt
import xarray as xr

# <div class="alert alert-info">
#     <h2>Show results</h2>
# </div>

import h5py

with h5py.File('output_sources/job_0001/gather0.h5', 'r') as f:
    print(f['receiver_ids_ACOUSTIC_point'][:])

# First, we load one of the HDF5 output file as an `xarray.Dataset`:

ds = xr.load_dataset('output_sources/job_0001/output_0001.h5', engine='h5netcdf')

# We now list the `xarray.DataArray`s included in the dataset:

ds.data_vars

# Print the dimensions and shape of each dataset:

for da in ds:
    print(da, ds[da].dims, ds[da].shape)

# We show the coordinates associated with the datasets:

ds.coords

# Print the dimensions and shape of each coordinate:

for coord in ds.coords:
    print(coord, ds.coords[coord].dims, ds.coords[coord].shape)

# Now we plot a time slice:

da = ds['slice_time_0000']

clip = 1e-9
vmin, vmax = -clip, clip
_ = da[da.time_slice==0.8, da.x>3000, :].T.plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# Now we plot a DFT slice:

da = ds['slice_dft_0000']

clip = 1e-2
vmin, vmax = -clip, clip
_ = da[da.freq_dft==20, :, da.x>400].plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# Finally, we plot a shot gather:

da = ds['gather_0000']

clip = 5e-1
vmin, vmax = -clip, clip
_ = da[1, :, :].plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)
# _ = da[0, :, 10].plot() #aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)
# plt.xlim([0.01, 0.05])


