# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: run forward simulations with Matterhorn</h1>
# </div>

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from collections import namedtuple

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
import segyio

# Imports for Cervino.
from cervino.core import create_workflow_dict, SolverSite, Cervino
from cervino.core import definitions

# <div class="alert alert-info">
#     <h2>Cervino setup</h2>
# </div>

# The `cervino` package defines a general `Cervino` class which allows one to do most of the work. Before creating an object of the `Cervino` class, we need to define a few parameters. First, we need to specify the workflow type. As a reminder, the workflows allowed by `cervino` are defined in the `core.definitions.workflow_valid` dictionary and more information about the specific workflows can be found [here](https://matterhorn-eth.gitlab.io/cervino/workflow.html).

pprint(definitions.workflows_valid.items())

pprint(definitions.tasks_valid.keys())

# Here, we pick a `forward` workflow:

workflow_type = 'forward'

# Then, we need to create a dictionary that specifies the solver and the site (local or cluster) for each pipe of the workflow. We use the `create_workflow_dict` utility to create an empty skeleton of this dictionary:

workflow_dict = create_workflow_dict(workflow_type)

pprint(workflow_dict)

# Above, we see that, [as expected](https://matterhorn-eth.gitlab.io/cervino/workflow.html), the `forward` workflow is composed by one pipe only, namely a `compute_forward_simulation` pipe. Now, we need to pick the solver and site for the specific pipe:

workflow_dict['compute_forward_simulation'] = SolverSite('salvus', 'aug04')

pprint(workflow_dict)

# Finally, we can define the `cervino` dictionary required to create a `cervino` workflow:

cervino_dict = {
    'workflow_type': workflow_type,
    'workflow_dict': workflow_dict,
    'name': 'marmousi_matterhorn',
    'desc': 'Simple Matterhorn forward simulation on the Marmousi model',
    'output_folder': 'output_sources',
}

pprint(cervino_dict)

# This is the `Cervino` object that we will use to perform the simulations:

marmousi = Cervino(**cervino_dict)

# Before performing and simulation with `cervino`, we need to create the dictionaries for the tasks that will compose the forward simulation pipe. This is done in another Notebook. Here, we load the tasks into the pipeline of the Cervino simulation:

tasks_from_pickle = 'tasks_sources.pkl'
marmousi.workflow.pipeline['compute_forward_simulation'].attach_tasks_from_pickle(tasks_from_pickle)

# The step above needs to be peformed for each pipe of the workflow. However, for a `forward` workflow, there is only one pipe (`compute_forward_simulation`), so for this example we need to perform the task above only once.

# The `summary()`method of the `Cervino` object prints a short summary of the `cervino` workflow we have built so far:

summary = marmousi.summary()
for line in summary:
    print(line)

# <div class="alert alert-info">
#     <h2>Perform workflow</h2>
# </div>

# We first need to define another small dictionary with a few required parameters:

run_dict = {
    'progress': True, # Explain
    'verbose': True, # Explain
    'n_ranks': 8, # Explain
    'max_ranks': 16, # Explain
}

result, output_files = marmousi.run(run_dict)

# +
# # !ls -altr salvus*.pkl
# -

import pickle

with open('salvus_0000.pkl', 'rb') as f:
    sd = pickle.load(f)

sd

# We can print the standard output (or standard error) produced by the simulation (this currently works for `Matterhorn` only):

# +
# # Choose a simulation
# i = 0
# # 0 for standard output and 1 for standard error
# out_err = 0 
# pprint(result['compute_forward_simulation'][i][out_err].decode("utf-8"))
# -

# The output files created by each taks are gathered into one larger HDF5 file (one file for each task):

pprint(output_files)


