# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %matplotlib inline

# +
import pyvista as pv
import numpy as np
from pyvista import examples


# make a 3D numpy array
# this is an example dataset already in a UniformGrid mesh
# just use `pv.wrap` to convert your 3D numpy array
# e.g.
# pv.wrap(my_3d_array)
# FYI: see https://docs.pyvista.org/examples/00-load/create-uniform-grid.html#sphx-glr-examples-00-load-create-uniform-grid-py

vol = examples.download_brain()
vol
# -

# interactive slicing in a pop out window
p = pv.Plotter(notebook=False) #, off_screen=False)
p.add_mesh_slice(vol)
p.show() #interactive=True, auto_close=False)

# do single slicing
slcx = vol.slice(normal='x', origin=(30, 20, 0))
slcy = vol.slice(normal='y', origin=(30, 20, 0))
slcx.plot()
slcy.plot()

# +
slc = vol.slice_orthogonal(x=10, y=20, z=30).combine()

# notebook plotting 
# slices aren't interactive though
# you can change the slice position by altering the filter paramaters
p = pv.PlotterITK()
p.add_mesh(slc)
p.show()
# -


