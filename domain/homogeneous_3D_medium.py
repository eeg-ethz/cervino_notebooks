# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

import xarray as xr
import segyio

pwd

dir_model = Path('model')
dir_output = Path("output/job_0000")

required_models = ['VP', 'RHO']
n = nx, ny, nz = (400, 300, 200)
d = dx, dy, dz = (4.0, 4.0, 4.0)
dd = {
    'x': (200, 200),
    'y': (150, 150),
    'z': (100, 100)
}

[j * k for j, k in zip(n, d)]

# ## Create model parameters

vp = np.ones(n, dtype=np.float32) * 2000.0
vp.transpose((1, 0, 2)).tofile(str(dir_model / f"h3d_{'VP'}.bin"))

rho = np.ones(n, dtype=np.float32) * 1000.0
rho.transpose((1, 0, 2)).tofile(str(dir_model / f"h3d_{'RHO'}.bin"))

# ## DFT coefficients

fn = 'dft_coeffs.bin'

np.ones((2,), dtype=np.complex64).tofile(fn)

# ## Plot gathers

fn = dir_output / 'gather0_sxx.su'
with segyio.su.open(fn, ignore_geometry=True, endian='little') as sufile:
    gather = sufile.trace.raw[:].T

plt.imshow(gather)

# ## Load DFT volumes

fn0 = dir_output / 'volume_dft_sxx_0.su'
fn1 = dir_output / 'volume_dft_sxx_1.su'
fn4 = dir_output / 'volume_dft_sxx_4.su'
fn5 = dir_output / 'volume_dft_sxx_5.su'

nf = 2

n2 = nx2, ny2, nz2 = (200, 150, 100)

dft0 = np.fromfile(str(fn0), dtype=np.complex64).reshape((nf, nz2, ny2, nx2))
dft1 = np.fromfile(str(fn1), dtype=np.complex64).reshape((nf, nz2, ny2, nx2))
dft4 = np.fromfile(str(fn4), dtype=np.complex64).reshape((nf, nz2, ny2, nx2))
dft5 = np.fromfile(str(fn5), dtype=np.complex64).reshape((nf, nz2, ny2, nx2))

clip = 1e3
vmin, vmax = -clip, clip
plt.imshow(dft0[1, :, 90, :].real, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

clip = 1e3
vmin, vmax = -clip, clip
plt.imshow(dft1[1, :, 90, :].real, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

clip = 1e3
vmin, vmax = -clip, clip
plt.imshow(dft4[1, :, 90, :].real, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

clip = 1e3
vmin, vmax = -clip, clip
plt.imshow(dft5[1, :, 90, :].real, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

import pyvista as pv
from pyvista import examples

# +
vol = examples.download_brain()

p = pv.Plotter(notebook=False)
p.add_mesh_clip_plane(vol)
p.show()
# -

vol = pv.wrap(dft0[1, :, :, :].real)

vol = pv.wrap(my_numpy_3d_ndarray)
p = pv.Plotter(notebook=False)
p.add_mesh_clip_plane(vol)
p.show()


