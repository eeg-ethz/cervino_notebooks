# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# # %matplotlib notebook
# %load_ext autoreload
# %autoreload 2

from pathlib import Path
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
mpl.rcParams['savefig.dpi'] = 80
mpl.rcParams['figure.dpi'] = 80

import ipywidgets as widgets
from ipywidgets import interact, interact_manual

import xarray as xr
import segyio

# %matplotlib notebook

pwd

dir_model = Path('model')
dir_output = Path("output") / "job_0000"

required_models = ['VP', 'RHO']
n = nx, ny, nz = (400, 300, 200)
d = dx, dy, dz = (4.0, 4.0, 4.0)

# ## Load DFT volumes

ds = xr.load_dataset(dir_output / 'output_0000.h5', engine='h5netcdf')

ds.data_vars

ds.coords

dft = ds['volume_dft_0000'][0]

dft.shape

# +
clip = 1e3
vmin, vmax = -clip, clip

nrows = 2
ncols = 2

height_ratios = [ny, nz]
width_ratios = [nx, ny]

fig_nh = ny + nz
fig_nw = nx + ny

fig_w = 9
fig_h = fig_w * (fig_nh / fig_nw)

fig = plt.figure(figsize=(fig_w, fig_h*1.01))
grid = plt.GridSpec(nrows, ncols, hspace=0.05, wspace=0.05, height_ratios=height_ratios, width_ratios=width_ratios)

# Bottom left - X-Z slice
ax_xz = fig.add_subplot(grid[1, 0])
# Top left - X-Y slice
ax_xy = fig.add_subplot(grid[0, 0], sharex=ax_xz)
# Bottom right - Y-Z slice
ax_yz = fig.add_subplot(grid[1, 1], sharey=ax_xz)

# ax_empty = fig.add_subplot(grid[0, 1], sharex=ax_yz, sharey=ax_xy)

ix, iy, iz = 10, 10, 10

# Bottom left - X-Z slice
im_xz = ax_xz.imshow(dft[1, :, iy, :].real, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)
sl_xz_x, = ax_xz.plot([iy, iy], [0, nz-1], color='k')
sl_xz_z, = ax_xz.plot([0, nx-1], [iz, iz], color='k')
ax_xz.set_xlabel('X [m]')
ax_xz.set_ylabel('Z [m]')

# Top left - X-Y slice
im_xy = ax_xy.imshow(dft[1, iz, :, :].real, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)
sl_xy_x, = ax_xy.plot([ix, ix], [0, ny-1], color='k')
sl_xy_y, = ax_xy.plot([0, nx-1], [iy, iy], color='k')
ax_xy.set_ylabel('Y [m]')

# Bottom right - Y-Z slice
im_yz = ax_yz.imshow(dft[1, :, :, ix].real, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)
sl_yz_y, = ax_yz.plot([iy, iy], [0, nz-1], color='k')
sl_yz_z, = ax_yz.plot([0, ny-1], [iz, iz], color='k')
ax_yz.set_xlabel('Y [m]')

# im_empty = ax_empty.imshow(np.zeros((ny, ny)), cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# grid.tight_layout(fig)
# plt.tight_layout()

# for iy in range(ncols):
#     for ix in range(nrows):
#         axes[ix, iy].set_xticklabels([])
#         axes[ix, iy].set_yticklabels([])

# # Remove horizontal space between axes
# fig.subplots_adjust(hspace=0, wspace=0)

# ax_xy.set_xticks([])
# ax_empty.set_xticks([])
# ax_empty.set_yticks([])
# ax_yz.set_yticks([])

ax_xy.axes.get_xaxis().set_visible(False)
# ax_xy.axes.get_yaxis().set_visible(False)
# ax_yz.axes.get_xaxis().set_visible(False)
ax_yz.axes.get_yaxis().set_visible(False)
# ax_empty.axes.get_xaxis().set_visible(False)
# ax_empty.axes.get_yaxis().set_visible(False)

# ax_xz.axes.get_xaxis().set_visible(False)
# ax_xz.axes.get_yaxis().set_visible(False)

ax_xz.set_aspect('equal')
ax_xy.set_aspect('equal')
ax_yz.set_aspect('equal')

plt.tight_layout()

def f(z=iz, y=iy, x=ix):
    # Bottom left - X-Z slice
    im_xz.set_data(dft[1, :, y, :].real)
    sl_xz_x.set_xdata([x, x])
    sl_xz_z.set_ydata([z, z])
    # Top left - X-Y slice
    im_xy.set_data(dft[1, z, :, :].real)
    sl_xy_x.set_xdata([x, x])
    sl_xy_y.set_ydata([y, y])
    # Bottom right - Y-z slice
    im_yz.set_data(dft[1, :, :, x].real)
    sl_yz_y.set_xdata([y, y])
    sl_yz_z.set_ydata([z, z])


# -

_ = interact(f, x = (0, nx-1), y = (0, ny-1), z = (0, nz-1))

# ## Plot gathers


gather = ds['gather_0001']

gather.shape

plt.figure()
gather[0, ...].plot(yincrease=False)
