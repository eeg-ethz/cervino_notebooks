# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

import xarray as xr

from cervino.solvers.matterhorn_to_cervino import _create_hdf5_from_matterhorn_binary

dir_model = Path('model')
if not dir_model.exists():
    Path.mkdir(dir_model)

required_models = ['VP', 'RHO']
n = nx, ny, nz = (400, 300, 200)
d = dx, dy, dz = (4.0, 4.0, 4.0)

# ## Create model parameters

vp = np.ones(n, dtype=np.float32) * 2000.0
vp.transpose((1, 0, 2)).tofile(str(dir_model / f"h3d_{'VP'}.bin"))

rho = np.ones(n, dtype=np.float32) * 1000.0
rho.transpose((1, 0, 2)).tofile(str(dir_model / f"h3d_{'RHO'}.bin"))

fn_h5 = str(dir_model / "h3d.h5")
_ = _create_hdf5_from_matterhorn_binary(
    filename_prefix=str(dir_model / "h3d"),
    fn_h5=fn_h5,
    required_models=required_models,
    n=n,
    d=d,
)

# +
# ds = xr.load_dataset(fn_h5, engine="h5netcdf")
