# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
import urllib.request
import tarfile
import zipfile

import xarray as xr

from cervino.solvers.matterhorn_to_cervino import _create_hdf5_from_matterhorn_binary

# [SEG/EAGE Salt and Overthrust Models](https://wiki.seg.org/wiki/SEG/EAGE_Salt_and_Overthrust_Models)

fn_url = "https://s3.amazonaws.com/open.source.geoscience/open_data/seg_eage_models_cd/Salt_Model_3D.tar.gz"

fn_archive = Path(Path(fn_url).name)

if not fn_archive.is_file():
    result = urllib.request.urlretrieve(str(fn_url), str(fn_archive))

tarfile.open(fn_archive, mode="r:gz").extractall()

fn_zip = zipfile.ZipFile("Salt_Model_3D/3-D_Salt_Model/VEL_GRIDS/SALTF.ZIP")

fn_binary = "Saltf@@"

result = fn_zip.extract(fn_binary)

Path(result).name

assert Path(result).name == fn_binary

# +
# fid = fopen(file);
# v = fread(fid,'float',0 ,'b'); % big endian
# fclose(fid);

# v = reshape(v,[nx ny nz]);
# %% Arrange dimensions for Matterhorn
# % The dimensions of are currently in this order: x, y, z
# % Matterhorn (written in C++, a `row-major` programming language)
# % requires that binary files contain data stored in this order: y, x, z
# % Since MATLAB is a `column-major` software, the data must be
# % reordered in this way: z, x, y

# % Before
# % 1, 2, 3
# % x, y, z

# % After
# % 3, 1, 2
# % z, x, y
# v = permute(v,[3 1 2]);
# %% Write data to binary file
# fid = fopen('model_salt3d_VP.bin', 'w', 'n');
# fwrite(fid, v, 'single');
# fclose(fid);

# rho = ones(size(v))*2500;
# fid = fopen('model_salt3d_RHO.bin', 'w', 'n');
# fwrite(fid, rho, 'single');
# fclose(fid);
# -

required_models = ['VP', 'RHO']
n = (676, 676, 210)
d = (20.0, 20.0, 20.0)

vp_from_binary = np.fromfile(fn_binary, dtype='>f4').astype(np.float32).reshape(n[::-1])

vp_from_binary.shape # z, y, x

plt.imshow(vp_from_binary[:, :, 300])

vp = vp_from_binary.transpose((1, 2, 0)) # y, x, z

plt.imshow(vp[:, 300, :].T)

rho = np.ones_like(vp) * 2500.0

dir_base = Path('model')
if not dir_base.exists():
    dir_base.mkdir()
filename_prefix = dir_base / 'seg_eage_salt_{}.bin'
print(filename_prefix)

vp.tofile(str(filename_prefix).format('VP'))
rho.tofile(str(filename_prefix).format('RHO'))

fn_h5 = str(dir_base / "seg_eage_salt.h5")
_ = _create_hdf5_from_matterhorn_binary(
    filename_prefix=str(dir_base / "seg_eage_salt"),
    fn_h5=fn_h5,
    required_models=required_models,
    n=n,
    d=d,
)

ds = xr.load_dataset(fn_h5, engine="h5netcdf")

ds.data_vars

ds['VP'][:, 400, :].T.plot(size=5, yincrease=False, aspect=3)
