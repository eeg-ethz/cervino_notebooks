# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: show results of forward simulation</h1>
# </div>

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %load_ext autoreload
# %autoreload 2

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt

import xarray as xr

# %matplotlib inline

# <div class="alert alert-info">
#     <h2>Useful dictionaries</h2>
# </div>

clip_time_dict = {
    'sxx': 5e+3,
    'vx': 5e-4,
    'vz': 5e-4,
}

clip_dft_dict = {
    'sxx': 1e+6,
}

# <div class="alert alert-info">
#     <h2>Load cervino's output file</h2>
# </div>

ds = xr.load_dataset(f"./output/job_0000/output_0000.h5", engine="h5netcdf")

ds.data_vars

# <div class="alert alert-info">
#     <h2>Show gathers</h2>
# </div>

fields_valid = {field: i for i, field in enumerate(ds.coords['fields_gather'].values)}
pprint(fields_valid)

da = ds['gather_0001']

field_string = 'vx'
field = fields_valid[field_string]

clip = clip_time_dict[field_string]
vmin, vmax = -clip, clip

_ = da[field, :, :].plot(aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

t = ds['time_gather']
recx = ds['recx']

# +
ir = 700
# print(f"The receiver is at x: {recx[ir].values}")

# Figure
f, ax = plt.subplots(nrows=3, ncols=1, figsize=(12, 12), squeeze=False)

lims = [1.0, 1.2]

field_string = 'vx'
field = fields_valid[field_string]

_ = ax[0, 0].plot(t, da[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
_ = ax[0, 0].set_xlim(lims)

field_string = 'vz'
field = fields_valid[field_string]

_ = ax[1, 0].plot(t, da[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[1, 0].legend()
_ = ax[1, 0].set_xlim(lims)

field_string = 'sxx'
field = fields_valid[field_string]

_ = ax[2, 0].plot(t, da[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[2, 0].legend()
_ = ax[2, 0].set_xlim(lims)
# -

# <div class="alert alert-info">
#     <h2>Show time wavefields</h2>
# </div>

fields_valid = {field: i for i, field in enumerate(ds.coords['fields_time_slice'].values)}
pprint(fields_valid)

da = ds['slice_time_0000']

it = 6

field_string = 'vx'
field = fields_valid[field_string]

clip = clip_time_dict[field_string]
vmin, vmax = -clip, clip

_ = da[field, it, da.x>30, :].T.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

x = ds['x']
z = ds['z']

# +
it = 6
ix = 750

# Figure
f, ax = plt.subplots(nrows=2, ncols=1, figsize=(12, 12), squeeze=False)

lims = [1500, 2000]

field_string = 'vx'
field = fields_valid[field_string]

_ = ax[0, 0].plot(z, da[field, it, ix, :], label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
# _ = ax[0, 0].set_xlim(lims)

field_string = 'vz'
field = fields_valid[field_string]

_ = ax[1, 0].plot(z, da[field, it, ix, :], label=f"Matterhorn {field_string}")
_ = ax[1, 0].legend()
# _ = ax[1, 0].set_xlim(lims)
# -
# <div class="alert alert-info">
#     <h2>Show frequency wavefields</h2>
# </div>

fields_valid = {field: i for i, field in enumerate(ds.coords['fields_dft_slice'].values)}
pprint(fields_valid)

da = ds['slice_dft_0000']

print(da.shape)

ifreq = 4

freqs = da['freq_dft'].values

omega = (2.0 * np.pi * freqs[ifreq])
print(omega)

field_string = 'sxx'
field = fields_valid[field_string]

clip = clip_dft_dict[field_string]
vmin, vmax = -clip, clip

_ = da[field, ifreq, :, da.x>30].real.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = da[field, ifreq, :, da.x>30].imag.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

x = ds['x']
z = ds['z']

# +
ix = 650

# Figure
f, ax = plt.subplots(nrows=2, ncols=1, figsize=(12, 10), squeeze=False)

lims = [1000, 2500]

field_string = 'sxx'
field = fields_valid[field_string]

_ = ax[0, 0].plot(z, da[field, ifreq, :, ix].real, label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
# _ = ax[0, 0].set_xlim(lims)

_ = ax[1, 0].plot(z, da[field, ifreq, :, ix].imag, label=f"Matterhorn {field_string}")
_ = ax[1, 0].legend()
# _ = ax[0, 0].set_xlim(lims)
