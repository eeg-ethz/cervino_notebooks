# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: run FWI workflow</h1>
# </div>

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from pathlib import Path
from pprint import pprint

# Imports for Cervino.
from cervino.core import create_workflow_dict, Cervino
from cervino.core import definitions
from cervino.utils.tools import Cluster
from cervino.utils.fwi import _create_submission_script_fwi_v1

# <div class="alert alert-info">
#     <h2>Cervino setup</h2>
# </div>

# The `cervino` package defines a general `Cervino` class which allows one to do most of the work. Before creating an object of the `Cervino` class, we need to define a few parameters. First, we need to specify the workflow type. As a reminder, the workflows allowed by `cervino` are defined in the `core.definitions.workflow_valid` dictionary and more information about the specific workflows can be found [here](https://eeg-ethz.gitlab.io/cervino/workflow.html).

pprint(definitions.workflows_valid.keys())

# Here, we pick a `forward` workflow:

workflow_type = 'forward'

# Then, we need to create a dictionary that specifies the solver and the site (local or cluster) for each job of the workflow. We use the `create_workflow_dict` utility to create an empty skeleton of this dictionary and then we print the empty dictionary:

workflow_dict = create_workflow_dict(workflow_type)
pprint(workflow_dict)

# Above, we see that, [as expected](https://eeg-ethz.gitlab.io/cervino/workflow.html), the `forward` workflow is composed by one job only, namely a `compute_forward_simulation` job. Now, we need to pick the solver and site for the specific job and provide them as a `tuple`:

workflow_dict['compute_forward_simulation'] = ('matterhorn', 'euler')
# These below are just examples for`workflow_type='inversion'`
# workflow_dict['compute_residuals'] = ('some_other_program', 'aug04')
# workflow_dict['compute_backward_simulation'] = ('matterhorn', 'euler')
# ...

pprint(workflow_dict)

# Finally, we can define the `cervino` dictionary required to create a `cervino` workflow:

# Define a remote folder
remote_folder = '/cluster/scratch/bfilippo/linan_workflow_test_v0'

# Define a dicionary for Step 1
# Note that the remote folder is a subfolder
cervino_dict_ws = {
    'workflow_type': workflow_type,
    'workflow_dict': workflow_dict,
    'name': 'marmousi_euler',
    'desc': 'Simple Matterhorn forward simulation on the Marmousi model',
    'remote_folder': f"{remote_folder}/ws",
    'output_folder': 'output_ws',
    'user': 'bfilippo',
}

# Define a dicionary for Step 2
# Note that the remote folder is a subfolder
cervino_dict_wr = {
    'workflow_type': workflow_type,
    'workflow_dict': workflow_dict,
    'name': 'marmousi_euler',
    'desc': 'Simple Matterhorn forward simulation on the Marmousi model',
    'remote_folder': f"{remote_folder}/wr",
    'output_folder': 'output_wr',
    'user': 'bfilippo',
}

pprint(cervino_dict_ws)
pprint(cervino_dict_wr)

# This is the `Cervino` object that we will use to perform the simulations:

marmousi_ws = Cervino(**cervino_dict_ws)

marmousi_wr = Cervino(**cervino_dict_wr)

# Before executing a simulation with `cervino`, we need to create the dictionaries for the tasks that will compose the `copmute_forward_simulation` job. This is done in another Notebook. Here, we *attach* the tasks to the corresponding job of the Cervino simulation:

# Step1
# %cd ws
# %run 2d_forward_workflow_compute_forward_simulation_create_dict_ws.ipynb
# %run 2d_receiver_locations_ws.ipynb
tasks_from_pickle_ws = 'compute_forward_simulation_tasks_ws.pkl'
marmousi_ws.attach_tasks_to_job_from_file('compute_forward_simulation', tasks_from_pickle_ws)
# %cd ..
# %pwd

# Step 2
# %cd wr
# %run 2d_forward_workflow_compute_forward_simulation_create_dict_wr.ipynb
# %run 2d_receiver_locations_wr.ipynb
tasks_from_pickle_wr = 'compute_forward_simulation_tasks_wr.pkl'
marmousi_wr.attach_tasks_to_job_from_file('compute_forward_simulation', tasks_from_pickle_wr)
# %cd ..
# %pwd

# The step above needs to be peformed for each job of the workflow. However, for a `forward` workflow, there is only one job (`compute_forward_simulation`), so for this example we need to perform the task above only once.

# The `summary()`method of the `Cervino` object prints a short summary of the `cervino` workflow we have built so far:

# Step 1
summary = marmousi_ws.summary()
for line in summary:
    print(line)

# Step 2
summary = marmousi_wr.summary()
for line in summary:
    print(line)

# <div class="alert alert-info">
#     <h2>Perform workflow</h2>
# </div>

# We first need to define another small dictionary with a few required parameters:

run_dict = {
    'progress': True, # Explain
    'verbose': True, # Explain
    'n_ranks': 16, # Explain: only for local simulations
    'max_ranks': 16, # Explain: only for local simulations
    'setup_only': True, # Create and transfer files but no not submit jobs to the scheduler
#     'transfer_domain_files': True,
}

# Step 1
# %cd ws
result_ws = marmousi_ws.run(run_dict)
# %cd ..

# Step 2
# %cd wr
result_wr = marmousi_wr.run(run_dict)
# %cd ..

# Create the submission script to run the FWI simulation on Euler:

fwi_dict = {
    # First and last index of FWI iterations
    'iter_beg': 1,
    'iter_end': 2,

    # Number of ranks for each step
    'nranks_step1': 2,
    'nranks_step2': 2,
    'nranks_step3': 1,

    # Number of cores for each step
    'ncores_step1': 24,
    'ncores_step2': 24,
    'ncores_step3': 1,

    # Job array first and last index for each step
    'job_beg_step1': 1,
    'job_end_step1': 2,
    'job_beg_step2': 1,
    'job_end_step2': 2,

    # Commands files for each step, see https://scicomp.ethz.ch/wiki/Job_arrays#Using_a_.22commands.22_file
    'commands_step1': Path('ws/commands'),
    'commands_step2': Path('wr/commands'),

    # Matterhorn input files for each step
    'input_files_step1': marmousi_ws._input_files['compute_forward_simulation'].values(),
    'input_files_step2': marmousi_wr._input_files['compute_forward_simulation'].values(),
}

# Create the submission script required to run the full FWI workflow:

fwi_files = _create_submission_script_fwi_v1('test.sh', fwi_dict)
print(fwi_files)

# %cat test.sh

# Create the cluster dictionary:

cluster_dict = {
    'user': 'bfilippo',
    'site': 'euler',
    'verbose': True
}

# Create the cluster object:

euler = Cluster(**cluster_dict)

# Connect to the cluster:

euler.open()

# Transfer the files to the cluster:

euler.transfer_files_to_cluster(fwi_files, remote_folder)

# Submit the FWI workflow to the cluster:

jobid_dict = euler.submit_jobs_to_cluster('test.sh', remote_folder, getid=False)

# After the submission is complete, a file named `jobit.txt` will be created. This file contains the JOBIDs of the submitted jobs. We check this file to obtain the status of the jobs:

stdin, stdout, stderr = euler._connection.exec_command(f"cd {remote_folder} && cat jobid.txt")

jobid_list = stdout.readlines()
jobid_dict = dict(zip(range(len(jobid_list)), jobid_list))
pprint(jobid_dict)

jobid_status_dict = euler.print_status_jobs(jobid_dict)

euler.close()

# ## The end!
