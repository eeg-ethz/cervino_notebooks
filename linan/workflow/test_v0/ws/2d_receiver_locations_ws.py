# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

# ## Create receiver locations

n = nx, ny, nz = (400, 1, 300)
d = dx, dy, dz = (4.0, 4.0, 4.0)

receiver_array = np.empty((nx, 3), dtype=np.float32)
ry0 = 0.0
rz0 = 30.0
for i in range(nx):
    receiver_array[i] = [i * dx, ry0, rz0]

# ## Save receiver locations to file

fn = 'receivers.txt'
np.savetxt(fn, receiver_array, delimiter=" ", fmt=['%.2f', '%.2f', '%.2f'])

# +
# # !cat receivers.txt
