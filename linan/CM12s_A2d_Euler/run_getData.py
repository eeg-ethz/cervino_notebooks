# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.0
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: run `forward` workflow</h1>
# </div>

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %matplotlib inline
# %load_ext autoreload
# %autoreload 2

from collections import namedtuple

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
import segyio
import os

# Imports for Cervino.
from cervino.core import create_workflow_dict, Cervino
from cervino.core import definitions

# <div class="alert alert-info">
#     <h2>Cervino setup</h2>
# </div>

# The `cervino` package defines a general `Cervino` class which allows one to do most of the work. Before creating an object of the `Cervino` class, we need to define a few parameters. First, we need to specify the workflow type. As a reminder, the workflows allowed by `cervino` are defined in the `core.definitions.workflow_valid` dictionary and more information about the specific workflows can be found [here](https://eeg-ethz.gitlab.io/cervino/workflow.html).

pprint(definitions.workflows_valid.keys())

# Here, we pick a `forward` workflow:

workflow_type = 'forward'

# Then, we need to create a dictionary that specifies the solver and the site (local or cluster) for each job of the workflow. We use the `create_workflow_dict` utility to create an empty skeleton of this dictionary and then we print the empty dictionary:

workflow_dict = create_workflow_dict(workflow_type)
pprint(workflow_dict)

# Above, we see that, [as expected](https://eeg-ethz.gitlab.io/cervino/workflow.html), the `forward` workflow is composed by one job only, namely a `compute_forward_simulation` job. Now, we need to pick the solver and site for the specific job and provide them as a `tuple`:

workflow_dict['compute_forward_simulation'] = ('matterhorn', 'euler')
# These below are just examples for`workflow_type='inversion'`
# workflow_dict['compute_residuals'] = ('some_other_program', 'aug04')
# workflow_dict['compute_backward_simulation'] = ('matterhorn', 'euler')
# ...

pprint(workflow_dict)

# Finally, we can define the `cervino` dictionary required to create a `cervino` workflow:

cervino_dict = {
    'workflow_type': workflow_type,
    'workflow_dict': workflow_dict,
    'name': 'marmousi_euler',
    'desc': 'Simple Matterhorn forward simulation on the Marmousi model',
    'remote_folder': '/cluster/scratch/lxu/test4',
    'output_folder': 'depot_SIMU/data_true/',
    'user': 'lxu',
}

pprint(cervino_dict)

# This is the `Cervino` object that we will use to perform the simulations:

marmousi = Cervino(**cervino_dict)

# Before executing a simulation with `cervino`, we need to create the dictionaries for the tasks that will compose the `copmute_forward_simulation` job. This is done in another Notebook. Here, we *attach* the tasks to the corresponding job of the Cervino simulation:

tasks_from_pickle = 'getWS_dict1.pkl'
marmousi.attach_tasks_to_job_from_file('compute_forward_simulation', tasks_from_pickle)

# The step above needs to be peformed for each job of the workflow. However, for a `forward` workflow, there is only one job (`compute_forward_simulation`), so for this example we need to perform the task above only once.

# The `summary()`method of the `Cervino` object prints a short summary of the `cervino` workflow we have built so far:

summary = marmousi.summary()
for line in summary:
    print(line)

# <div class="alert alert-info">
#     <h2>Perform workflow</h2>
# </div>

# We first need to define another small dictionary with a few required parameters:

run_dict = {
    'progress': True, # Explain
    'verbose': True, # Explain
    'n_ranks': 16, # Explain
    'max_ranks': 16, # Explain
}

result = marmousi.run(run_dict)

marmousi._output_files

# +
# marmousi.print_status_cluster()

# +
# marmousi.prepare_output_on_cluster()

# +
# marmousi.fetch_from_cluster()
# -

# We can print the standard output (or standard error) produced by the simulation (this currently works for `Matterhorn` only):

# +
# # Choose a simulation
# i = 0
# # 0 for standard output and 1 for standard error
# out_err = 0 
# pprint(result['compute_forward_simulation'][i][out_err].decode("utf-8"))
# -

# The output files created by each taks are gathered into one larger HDF5 file (one file for each task):

pprint(marmousi._output_files)
