# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.4.1
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# <div class="alert alert-info">
#     <h1>Cervino: MATLAB on Euler</h1>
# </div>

# <div class="alert alert-info">
#     <h2>Import packages and IPython magic commands</h2>
# </div>

# %load_ext autoreload
# %autoreload 2

# Imports for Cervino.
from cervino.utils.tools import Cluster

# <div class="alert alert-info">
#     <h2>Cervino setup</h2>
# </div>

cluster_dict = {
    'user': 'bfilippo',
    'site': 'euler',
    'verbose': True
}

euler = Cluster(**cluster_dict)

# <div class="alert alert-info">
#     <h2>Create scripts</h2>
# </div>

# +
# %%writefile submission.sh
#!/bin/bash

# Load modules
source /cluster/apps/local/env2lmod.sh
module load matlab/9.3

# Submit job to the scheduler
bsub -n 1 -J "matlab" -o matlab.out -e matlab.err -R "rusage[mem=512]" "matlab -nodisplay -nojvm -singleCompThread -r matrix_determinant"

# +
# %%writefile submission_parpool.sh
#!/bin/bash

# Load modules
source /cluster/apps/local/env2lmod.sh
module load matlab/9.3

# Submit job to the scheduler
bsub -n 24 -J "matlab_p" -o matlab_p.out -e matlab_p.err -R "rusage[mem=2048]" "matlab -nodisplay -singleCompThread -r squares_parpool"
# -

# %%writefile matrix_determinant.m
A = [1 -2 4; -5 2 0; 1 0 3]
d = det(A)

# +
# %%writefile squares_parpool.m
euler = parcluster('local');
squares = zeros(10, 1);
pool = parpool(euler, 24);
time_start = tic;
parfor i = 1:100000
	squares(i) = i^2;
end
time_end = toc(time_start);
disp(squares);
disp(['The elapsed time is ', num2str(time_end)]);
pool.delete()

mkdir('subdir')
fid = fopen('subdir/test.txt', 'wt');
fprintf(fid, 'Hello world!');
fclose(fid);
# -

# !ls

# <div class="alert alert-info">
#     <h2>Perform workflow</h2>
# </div>

# List of files to transfer to the cluster:

fn = [
    'submission.sh',
    'matrix_determinant.m',
    'submission_parpool.sh',
    'squares_parpool.m'
]

# Remote folder on the cluster where all the files will be transferred to:

remote_folder = '/cluster/scratch/bfilippo/tools_matlab_v2'

# Establish a connection to the cluster:

euler.open()

# Transfer files to the cluster:

euler.transfer_files_to_cluster(fn, remote_folder)

# Submit one or more jobs to the scheduler:

jobid_dict = euler.submit_jobs_to_cluster('submission_parpool.sh', remote_folder)

# +
# jobid_dict
# -

# Check status of the job(s):

jobid_status_dict = euler.print_status_jobs(jobid_dict)

# +
# jobid_status_dict
# -

# List of files to retrieve from the cluster:

fn_cluster = [
    'subdir/test.txt',
]

# Local folder where all the files will be transferred to:

# +
# local_folder = '.'
# -

# Transfer files from the cluster:

euler.transfer_files_from_cluster(fn_cluster, remote_folder, local_folder)

# !cat subdir/test.txt

# Close the connection:

euler.close()
