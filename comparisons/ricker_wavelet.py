# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---



import numpy as np
import matplotlib.pyplot as plt

from cervino.input.wavelets import RickerWavelet, UserWavelet

from salvus.flow import simple_config as config

# %matplotlib notebook

nt = 5000
dt = 4e-4
t = np.arange(nt) * dt

center_frequency = 20.0
delay_in_cycles = 1.5
time_shift_in_seconds = delay_in_cycles / center_frequency

t1 = t - time_shift_in_seconds

stf_m = RickerWavelet(t1, fc=center_frequency, delay=delay_in_cycles, delay_type='cycles')
stf_m1 = stf_m.wavelet

stf_s = config.stf.Ricker(center_frequency=center_frequency) #, time_shift_in_seconds=time_shift_in_seconds)
ts, stf_s1 = stf_s.get_stf()

ts = ts - ts[0]

f = plt.figure(figsize=(9, 5))
_ = plt.plot(t1, stf_m1, label='Matterhorn')
_ = plt.plot(ts, stf_s1, label='Salvus')
_ = plt.legend()
_ = plt.xlim([-0.1, 0.2])


