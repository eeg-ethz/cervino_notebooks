# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# %load_ext autoreload
# %autoreload 2

from collections import namedtuple

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt
import segyio
import h5py

from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator

import xarray as xr

from salvus_toolbox import toolbox

# %matplotlib inline

# # Gathers

cmp = 1

# ## Matterhorn

dsm = xr.load_dataset('../cervino/output/job_0000/output_0000.h5', engine="h5netcdf")

dsm.data_vars

dsm.coords

dam = dsm['gather_0000']

clip = 5e-4
vmin, vmax = -clip, clip
# _ = da[da.time_gather>0.1, da.recx>1000].plot(aspect="auto", figsize=(15, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)
_ = dam[cmp, :, (dam.recx>=300) & (dam.recx<=8900)].plot(aspect="auto", figsize=(9, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# ## Salvus

# +
# Generate a shotgather from and HDF5 receiver file.
das, dt, extent = toolbox.get_shotgather("../salvus/output/job_0000/gather.h5", cmp=cmp, field="gradient-of-phi")
print(f"dt is {dt}s")
print(das.shape)

# Normalize and plot the shotgather.
clip_min = -0.2 * das.max()
clip_max = +0.2 * das.max()
f = plt.figure(figsize=(9, 5))
plt.imshow(das, vmin=clip_min, vmax=clip_max, extent=extent, aspect="auto", cmap=plt.cm.seismic)

plt.xlabel("Offset (m)")
plt.ylabel("Time (s)")
plt.show()
# -

a = [0, 0, 0, 1, 2, 3, 3]
a.

field = "gradient-of-phi"

with h5py.File("../salvus/output_only_volume_many/job_0000/gather0.h5", 'r') as f:
    receivers = f['coordinates_ACOUSTIC_point'][:, :]
    idt = f['point'].attrs['sampling_interval_in_time_steps']
    t0 = f['point'].attrs['start_time_in_seconds']
    dts = 1.0 / f['point'].attrs['sampling_rate_in_hertz']
    das = f[f"point/{field}"][:, cmp, :]

index_sorted = receivers[:, 0].argsort()

plt.plot(receivers[:, 0]) #.argsort()

receivers_sorted = receivers[index_sorted, :]

das_sorted = das[index_sorted, :].T

plt.plot(receivers_sorted[:, 0] - np.arange(2301) * 4.0)
# plt.plot(np.arange(2301) * 4.0)

receivers_sorted

print(dam.shape)
print(das_sorted.shape)

plt.imshow(das_sorted)

dtm = dt
# dts = dts
tm = np.arange(dam.shape[1]) * dtm
ts = np.arange(das.shape[0]) * dts

dxm = 4
xm = np.arange(dam.shape[2]) * dxm
xs = receivers[:, 0]

if cmp == 0:
    das = -das
irm = 1020
irs = irm - 204//4
print(f"m: {xm[irm]}, s: {xs[irs]}")
f = plt.figure(figsize=(9, 5))
coeff = max(das[:, irs])/max(dam[cmp, :, irm])
# coeff = 2010.2874755859375
print(f"The normalization coefficient is {coeff.data}")
_ = plt.plot(tm, dam[cmp, :, irm] * coeff, label='Matterhorn')
_ = plt.plot(ts, (das[:, irs]), label='Salvus')
_ = plt.legend()
# _ = plt.xlim([0.3, 0.5])

# # Time wavefields

cmp = 1

dt = 4e-4
it = 20
# itm = it * dt / 200
# print(itm)

# ## Matterhorn

dam = dsm['slice_time_0000'] * 2000

dam.shape

# +
# dam.time_slice[it]
# -

clip = 1e0
vmin, vmax = -clip, clip
_ = dam[cmp, it, dam.x>300, :].T.plot(add_colorbar=False, aspect="auto", figsize=(9, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

# ## Salvus

# with h5py.File("../salvus/output/job_0000/wavefield.h5", 'r') as f:
with h5py.File("../salvus/output/wavefield.h5", 'r') as f:
    coords = f['coordinates_ACOUSTIC'][:]
    das = f['volume']['gradient-of-phi'][:, :, :, :25]
    idt = f['volume'].attrs['sampling_interval_in_time_steps']
    t0 = f['volume'].attrs['start_time_in_seconds']
#     dt = f['volume'].attrs['sampling_rate_in_hertz']

print(f"sampling_interval_in_time_steps: {idt}")
print(f"start_time_in_seconds {t0}")
# print(f"sampling_rate_in_hz {dt}")

coords.shape

das.shape

coords2 = coords[:, :, :].reshape((-1, 2))
das2 = das[:, :, cmp, :].reshape((das.shape[0], -1))
print(coords2.shape)
print(das2.shape)

linInter = LinearNDInterpolator(coords2[:, :], das2[it, :])

nearInter = NearestNDInterpolator(coords2[:, :], das2[it, :])

x_min, x_max, dx = 0, 2301*4, 4
y_min, y_max, dy = 0, 751*4, 4

grid_x, grid_y = np.mgrid[x_min:x_max:dx, y_min:y_max:dy]

values_interp = linInter(grid_x, grid_y)

values_interp = nearInter(grid_x, grid_y)

methods = [None, 'none', 'nearest', 'bilinear', 'bicubic', 'spline16',
           'spline36', 'hanning', 'hamming', 'hermite', 'kaiser', 'quadric',
           'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc', 'lanczos']

fig = plt.figure(figsize=(9, 10))
ax = fig.subplots(nrows=1, ncols=1, squeeze=False)
clip = 1e0
vmin, vmax = -clip, clip
ax[0, 0].imshow(values_interp.T, vmin=vmin, vmax=vmax, cmap=plt.cm.seismic, interpolation=methods[1])
ax[0, 0].invert_yaxis()
# ax[0, 0].set_xlim(400, 600)
# ax[0, 0].set_ylim(500, )

tmps = values_interp

tmpm = dam[cmp, it, :, :]

tmps.shape, tmpm.shape

z = np.arange(751) * 4

ir = 800

f = plt.figure(figsize=(9, 5))
# coeff = 2005.0543212890625
coeff = 1 #2000
_ = plt.plot(z, tmpm[ir, :] * coeff, label='Matterhorn')
_ = plt.plot(z, tmps[ir, ::-1], label='Salvus')
_ = plt.legend()
# _ = plt.xlim([0.2, 1.3])


