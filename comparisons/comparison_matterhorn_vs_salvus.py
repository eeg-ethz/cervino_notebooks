# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.2
#   kernelspec:
#     display_name: Python (cervino)
#     language: python
#     name: cervino
# ---

# # Comparison: Matterhorn and Salvus

# %load_ext autoreload
# %autoreload 2

from pprint import pprint

import numpy as np
import matplotlib.pyplot as plt

import xarray as xr

# %matplotlib inline

# ## Conversion dictionaries

clip_time_dict = {
    'sxx': 5e+3,
    'vx': 5e-4,
    'vz': 5e-4,
}

clip_dft_dict = {
    'sxx': 1e+6,
}

rho0 = 2000.0

coeff = {
    'sxx': 1.0,
    'vx': -1.0 / rho0,
    'vz': 1.0/ rho0,
}

# ## Load cervino output files for gathers and time slices

dsm = xr.load_dataset(f"../cervino/output_mh_time/job_0000/output_0000.h5", engine="h5netcdf")

dss = xr.load_dataset(f"../cervino/output_salvus_time/job_0000/output_0000.h5", engine="h5netcdf")

dsm.data_vars

dss.data_vars

# ## Gathers

fields_valid = {field: i for i, field in enumerate(dsm.coords['fields_gather'].values)}
pprint(fields_valid)

dam = dsm['gather_0000']
das = dss['gather_0000']

field_string = 'vx'
field = fields_valid[field_string]
c = coeff[field_string]

clip = clip_time_dict[field_string]
vmin, vmax = -clip, clip

_ = dam[field, :, :].plot(aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = (c * das[field, :, :]).plot(aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

t = dsm['time_gather']
recx = dsm['recx']

# +
ir = 700
# print(f"The receiver is at x: {recx[ir].values}")

# Figure
f, ax = plt.subplots(nrows=3, ncols=1, figsize=(12, 12), squeeze=False)

lims = [0.7, 1.0]

field_string = 'vx'
field = fields_valid[field_string]
c = coeff[field_string]

# Matterhorn
_ = ax[0, 0].plot(t, dam[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
_ = ax[0, 0].set_xlim(lims)

# Salvus
_ = ax[0, 0].plot(t, c * das[field, :, ir], label=f"Salvus {field_string}", marker='.')
_ = ax[0, 0].legend()
_ = ax[0, 0].set_xlim(lims)

field_string = 'vz'
field = fields_valid[field_string]
c = coeff[field_string]

# Matterhorn
_ = ax[1, 0].plot(t, dam[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[1, 0].legend()
_ = ax[1, 0].set_xlim(lims)

# Salvus
_ = ax[1, 0].plot(t, c * das[field, :, ir], label=f"Salvus {field_string}", marker='.')
_ = ax[1, 0].legend()
_ = ax[1, 0].set_xlim(lims)

field_string = 'sxx'
field = fields_valid[field_string]
c = coeff[field_string]

# Matterhorn
_ = ax[2, 0].plot(t, dam[field, :, ir], label=f"Matterhorn {field_string}")
_ = ax[2, 0].legend()
_ = ax[2, 0].set_xlim(lims)

# Salvus
_ = ax[2, 0].plot(t, c * das[field, :, ir], label=f"Salvus {field_string}", marker='.')
_ = ax[2, 0].legend()
_ = ax[2, 0].set_xlim(lims)
# -

# ## Time wavefields

fields_valid = {field: i for i, field in enumerate(dsm.coords['fields_time_slice'].values)}
pprint(fields_valid)

dam = dsm['slice_time_0000']
das = dss['slice_time_0000']

it = 4

field_string = 'vx'
field = fields_valid[field_string]
c = coeff[field_string]

clip = clip_time_dict[field_string]
vmin, vmax = -clip, clip

_ = dam[field, it, dam.x>30, :].T.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = (c * das[field, it, dam.x>30, :]).T.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=True, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

x = dsm['x']
z = dsm['z']

# +
it = 4
ix = 750

# Figure
f, ax = plt.subplots(nrows=2, ncols=1, figsize=(12, 12), squeeze=False)

lims = [1500, 2000]

field_string = 'vx'
field = fields_valid[field_string]
c = coeff[field_string]

# Matterhorn
_ = ax[0, 0].plot(z, dam[field, it, ix, :], label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
_ = ax[0, 0].set_xlim(lims)

# Salvus
_ = ax[0, 0].plot(z, c * das[field, it, ix, ::-1], label=f"Salvus {field_string}", marker='.')
_ = ax[0, 0].legend()
_ = ax[0, 0].set_xlim(lims)

field_string = 'vz'
field = fields_valid[field_string]
c = coeff[field_string]

# Matterhorn
_ = ax[1, 0].plot(z, dam[field, it, ix, :], label=f"Matterhorn {field_string}")
_ = ax[1, 0].legend()
_ = ax[1, 0].set_xlim(lims)

# Salvus
_ = ax[1, 0].plot(z, c * das[field, it, ix, ::-1], label=f"Salvus {field_string}", marker='.')
_ = ax[1, 0].legend()
_ = ax[1, 0].set_xlim(lims)
# -
# ## Load cervino output files for DFT slices

slice_type = 'dft'

dsm = xr.load_dataset(f"../cervino/output_mh_{slice_type}_no_gf/job_0000/output_0000.h5", engine="h5netcdf")

dss = xr.load_dataset(f"../cervino/output_salvus_{slice_type}_no_gf/job_0000/output_0000.h5", engine="h5netcdf")

dsm.data_vars

dss.data_vars

# ## DFT wavefields

fields_valid = {field: i for i, field in enumerate(dsm.coords['fields_dft_slice'].values)}
pprint(fields_valid)

dam = dsm['slice_dft_0000']
das = dss['slice_dft_0000']

print(dam.shape)
print(das.shape)

ifreq = 4

freqs = dam['freq_dft'].values

omega = (2.0 * np.pi * freqs[ifreq])
print(omega)

field_string = 'sxx'
field = fields_valid[field_string]
c = coeff[field_string]

clip = clip_dft_dict[field_string]
vmin, vmax = -clip, clip

_ = dam[field, ifreq, :, dam.x>30].real.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = das[field, ifreq, dam.x>30, :].real.T.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=True, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = dam[field, ifreq, :, dam.x>30].imag.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=False, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

_ = das[field, ifreq, dam.x>30, :].imag.T.plot(add_colorbar=False, aspect="auto", figsize=(12, 5), yincrease=True, cmap=plt.cm.seismic, vmin=vmin, vmax=vmax)

x = dsm['x']
z = dsm['z']

# +
ix = 650

# Figure
f, ax = plt.subplots(nrows=2, ncols=1, figsize=(12, 10), squeeze=False)

lims = [1000, 2500]

field_string = 'sxx'
field = fields_valid[field_string]
c = coeff[field_string]

# Matterhorn
_ = ax[0, 0].plot(z, dam[field, ifreq, :, ix].real, label=f"Matterhorn {field_string}")
_ = ax[0, 0].legend()
# _ = ax[0, 0].set_xlim(lims)

# Salvus
_ = ax[0, 0].plot(z, das[field, ifreq, ix, ::-1].real, label=f"Salvus {field_string}", marker='.')
_ = ax[0, 0].legend()
# _ = ax[0, 0].set_xlim(lims)

# Matterhorn
_ = ax[1, 0].plot(z, dam[field, ifreq, :, ix].imag, label=f"Matterhorn {field_string}")
_ = ax[1, 0].legend()
# _ = ax[0, 0].set_xlim(lims)

# Salvus
_ = ax[1, 0].plot(z, das[field, ifreq, ix, ::-1].imag, label=f"Salvus {field_string}", marker='.')
_ = ax[1, 0].legend()
# _ = ax[0, 0].set_xlim(lims)
